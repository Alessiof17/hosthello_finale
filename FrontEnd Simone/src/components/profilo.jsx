import React from "react";
import { Link } from "react-router-dom";

const Profilo = (props) => {
  return (
    <div className="container">
      <br></br>
      <div className="row align-items-start">
        <div className="col-md-3 ">
          <Link
            to="/:userId/gestioneprofilo"
            style={{
              textDecoration: "none",
              borderTopLeftRadius: "10px",
              borderTopRightRadius: "10px",
            }}
          >
            <button className="btn btn-outline-danger list-group-item list-group-item-action active">
              Dettagli Profilo
            </button>
          </Link>
          <Link
            to="/cronologiaguadagni"
            style={{
              textDecoration: "none",
              borderBottomLeftRadius: "10px",
              borderBottomRightRadius: "10px",
            }}
          >
            <button className="btn btn-outline-danger list-group-item list-group-item-action">
              Cronologia guadagni
            </button>
          </Link>
        </div>

        <div className="col-md-9">
          <div className="card">
            <div className="card-body">
              <div className="row">
                <div className="col-md-12">
                  <Link
                    to={`/modificaprofilo/${props.id}`}
                    className="float-right btn btn-outline-danger btn-sm"
                  >
                    Modifica Profilo
                  </Link>
                  <h4>Il Tuo Profilo</h4>

                  <hr />
                </div>
              </div>
              <p className="card-text">
                Piccola descrizione da referenziare dalla bio
              </p>
              <div className="row">
                <div className="col-12">
                  <div>
                    <label className="col-8 col-form-label">User Name:</label>
                    {props.username}
                  </div>
                  <div>
                    <label className="col-8 col-form-label">Nome:</label>
                    {props.nome}
                  </div>
                  <div>
                    <label className="col-8 col-form-label">Cognome:</label>
                    {props.cognome}
                  </div>
                  <div>
                    <label className="col-8 col-form-label">Email:</label>
                    {props.email}
                  </div>
                  <div>
                    <label className="col-8 col-form-label">
                      Data di nascita
                    </label>
                    {props.data_nascita}
                  </div>
                  <div>
                    <label className="col-8 col-form-label">Città</label>
                    {props.citta}
                  </div>
                  <div>
                    <label className="col-8 col-form-label">
                      Numero di telefono
                    </label>
                    {props.telefono}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Profilo;
