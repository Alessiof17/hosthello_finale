import React from "react";
import { Link } from "react-router-dom";

import { useForm } from "../util/form-hook";
import Input from "../elementiform/input-searchbar";
import {
  VALIDATOR_MAX,
  VALIDATOR_MIN,
  VALIDATOR_REQUIRE,
} from "../util/validators";

export default function BarraFiltri() {
  const [formState, inputHandler] = useForm({}, false);

  const InputUtente = (event) => {
    event.preventDefault();
    console.log(formState.inputs);
  };

  return (

      <div className="row justify-content-center">
        <div className="col-md-2 ">
          <h6
            className="mt-3 text-center"
            style={{ textShadow: "1px 1.5px 3px black", color: "white" }}
          >
            Numero Camere
          </h6>

          <Input
            id="numero_camere"
            element="input"
            type="number"
            validators={[VALIDATOR_REQUIRE]}
            errorText="Numero camere non valido."
            onInput={inputHandler}
            placeholder={"2"}
            style={{ textAlign: "center" }}
          />
        </div>
        <div className="col-md-2 ">
          <h6
            className="mt-3 text-center"
            style={{ textShadow: "1px 1.5px 3px black", color: "white" }}
          >
            Numero bagni
          </h6>

          <Input
            id="numero_bagni"
            element="input"
            type="number"
            validators={[VALIDATOR_REQUIRE()]}
            errorText="Numero bagni non valido."
            placeholder={"1"}
            onInput={inputHandler}
            style={{ textAlign: "center" }}
          />
        </div>
        <div className="col-md-2 ">
          <h6
            className="mt-3 text-center"
            style={{ textShadow: "1px 1.5px 3px black", color: "white" }}
          >
            Ospiti
          </h6>

          <Input
            id="num_ospiti"
            element="input"
            type="number"
            min="0"
            max="20"
            validators={[VALIDATOR_MAX(20), VALIDATOR_MIN(1)]}
            errorText="Errato"
            onInput={inputHandler}
            placeholder={"2"}
            style={{ textAlign: "center" }}
          />
        </div>
        <div className="col-lg-1 p-0 ">
          <Link style={{ textDecoration: "none" }} to="/ricerca">
            <button
              name="submit"
              className="btn btn-danger btn-block"
              onClick={InputUtente}
              style={{ marginTop: "43px" }}
            >
              Applica
            </button>
          </Link>
        </div>
      </div>

  );
}

