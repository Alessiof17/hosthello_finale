import React, { useContext } from "react";
import { Navbar, Nav, NavDropdown } from "react-bootstrap";

import { AuthContext } from "../../context/auth-context";
import "../../assets/css/navbar.css";

const NavBar = () => {
  return (
    <nav className="sticky-nav">
      <Navbar id="navbar" stcollapseOnSelect expand="lg">
        <Navbar.Brand
          style={{ fontWeight: "600", color: "#343432", fontSize: "30px" }}
          href="/"
        >
          <img
            src={"https://i.ibb.co/Qrd0C4d/logo512.png"}
            style={{ height: 40, marginBottom: 10 }}
            alt="logo"
          />
          ostHello
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="mr-auto"></Nav>
          <Nav style={{ fontSize: "18px" }}>
            <Nav.Link href="/">Home</Nav.Link>

            <NavDropdown title="Area Riservata" id="collasible-nav-dropdown">
              <NavDropdown.Item
                id="itemdropdown"
                href={"/:userId/gestioneprofilo"}
              >
                Gestione Profilo
              </NavDropdown.Item>
              <NavDropdown.Divider />
              <NavDropdown.Item
                id="itemdropdown"
                href="/:userId/gestioneimmobili"
              >
                Gestione Immobili
              </NavDropdown.Item>
            </NavDropdown>
            <NavDropdown title="Burocrazia" id="collasible-nav-dropdown">
              <NavDropdown.Item id="itemdropdown" href="/prova">
                Inserisci Dati
              </NavDropdown.Item>
              <NavDropdown.Divider />
              <NavDropdown.Item
                id="itemdropdown"
                href="/:userId/notificaturismo"
              >
                Notifica Turismo
              </NavDropdown.Item>
            </NavDropdown>
            <Nav.Link href="accedi">Accedi</Nav.Link>
            <Nav.Link href="/" onClick={""}>
              Logout
            </Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    </nav>
  );
};

export default NavBar;

//{auth.isLoggedIn && (
//)}

//{!auth.isLoggedIn && <Nav.Link href="accedi">Accedi</Nav.Link>}
//{auth.isLoggedIn && <Nav.Link href="/" onClick={auth.logout}>Logout</Nav.Link>}
//const auth = useContext(AuthContext);
