import React from "react";
import { Link } from "react-router-dom";

const PaginaNonTrovata = () => {
  return (
    <div className="container">
      <div className="row justify-content-center" >
            <div className="card" style={{ width: "18em", marginTop:"100px" }}>
              <div className="card-body">
                <div className="col-md-12 text-center">
                  <h4>404</h4>
                  <hr />
                  <p style={{ fontSize: "25px" }}>Pagina non trovata</p>
                </div>
              </div>
              <Link to="/" className="btn btn-lg btn-outline-danger">
                Home
              </Link>
            </div>
          </div>
        </div>

  );
};

export default PaginaNonTrovata;
