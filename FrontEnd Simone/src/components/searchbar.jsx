import React from "react";
import { Link } from "react-router-dom";

import { useForm } from "../util/form-hook";
import Input from "../elementiform/input-searchbar";
import {
  VALIDATOR_MAX,
  VALIDATOR_MIN,
  VALIDATOR_REQUIRE,
} from "../util/validators";

export default function SearchBar() {
  const [formState, inputHandler] = useForm({}, false);

  const InputUtente = (event) => {
    event.preventDefault();
    console.log(formState.inputs);
  };

  return (
    <form onSubmit={InputUtente}>
    <div className="col-md-12 d-flex flex-column justify-content-center">
      <div className="row">
        <div className="col ">
          <h6
            className="mt-3 text-center"
            style={{ textShadow: "1px 1.5px 3px black", color: "white" }}
          >
            Destinazione
          </h6>

          <Input
            id="destinazione"
            element="input"
            type="text"
            validators={[VALIDATOR_REQUIRE]}
            errorText="Inserisci una destinazione"
            onInput={inputHandler}
            placeholder={"Roma, Napoli, Milano..."}
          />
        </div>
        <div className="col-lg-2 ">
          <h6
            className="mt-3 text-center"
            style={{ textShadow: "1px 1.5px 3px black", color: "white" }}
          >
            Tipo di alloggio
          </h6>

          <Input
            id="tipo_struttura"
            element="select"
            type="text"
            validators={[VALIDATOR_REQUIRE()]}
            errorText="Seleziona"
            onInput={inputHandler}
          />
        </div>
        <div className="col-lg-2 ">
          <h6
            className="mt-3 text-center"
            style={{ textShadow: "1px 1.5px 3px black", color: "white" }}
          >
            Data di Partenza
          </h6>

          <Input
            id="data_partenza"
            element="input"
            type="date"
            validators={[VALIDATOR_REQUIRE()]}
            errorText="Inserisci data"
            onInput={inputHandler}
            placeholder={"Data Partenza"}
          />
        </div>
        <div className="col-lg-2 ">
          <h6
            className="mt-3 text-center"
            style={{ textShadow: "1px 1.5px 3px black", color: "white" }}
          >
            Data di Arrivo
          </h6>

          <Input
            id="data_arrivo"
            element="input"
            type="date"
            validators={[VALIDATOR_REQUIRE()]}
            errorText="Inserisci data"
            onInput={inputHandler}
            placeholder={"Data Partenza"}
          />
        </div>
        <div className="col-lg-1 ">
          <h6
            className="mt-3 text-center"
            style={{ textShadow: "1px 1.5px 3px black", color: "white" }}
          >
            Ospiti
          </h6>

          <Input
            id="num_ospiti"
            element="input"
            type="number"
            min="0"
            max="20"
            validators={[VALIDATOR_MAX(20), VALIDATOR_MIN(1)]}
            errorText="Errato"
            onInput={inputHandler}
            placeholder={"2"}
            style={{ textAlign: "center" }}
          />
        </div>
        <div className="col-lg-1 ">

          <h6
            className="mt-3 text-center"
            style={{ textShadow: "1px 1.5px 3px black", color: "white" }}
          >
            Prezzo
          </h6>

          <Input
            id="prezzo"
            element="input"
            type="text"
            min="0"
            validators={[]}
            onInput={inputHandler}
            placeholder={"100€"}
            style={{ textAlign: "center" }}
            
          />

        </div>
        <div className="col-lg-1 p-0 ">
          <Link to="/ricerca" style={{textDecoration: "none"}}>
            <button
              name="submit"
              className="btn btn-danger btn-block"
              style={{ marginTop: "43px" }}
              disabled={!formState.isValid}

            >
              Cerca
            </button>
          </Link> 
        </div>
      </div>
    </div>
    </form>
  );
}

