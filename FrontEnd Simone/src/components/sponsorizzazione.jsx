import React from "react";
import { Link } from "react-router-dom";

const Sponsorizzazione = (props) => {

  return (
    <div>
      <div className="row justify-content-center">
        <div className="col-md-8">
          <div className="card">
            <div
              className="row card-body p-0 m-0"
              style={{ boxShadow: "0px 15px 15px rgb(112, 57, 6)" }}
            >
              <div className="col-md p-0">
                <h3
                  className="card-header text-center"
                  style={{ color: "#ff8400" }}
                >
                  {props.nome}
                  <hr />
                  
                    <h4 className="card-body text-center">
                    <Link style={{ textDecoration: "none" }}>
                      <button className="btn btn-outline-danger" onClick={""}>
                        {props.durata} <br></br> {props.costo}€
                      </button>
                      </Link>
                    </h4>
                  
                </h3>
              </div>
            </div>
          </div>
        </div>
      </div>
      <br />
    </div>
  );
};

export default Sponsorizzazione;
