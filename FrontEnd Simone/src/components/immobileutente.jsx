import React from "react";
import { Link } from 'react-router-dom';


const ImmobileUtente = props => {

    return (
        <div>
            <div className="row">
                <div className="col-lg-12">
                    <div className="card">
                        <div className="row card-body">
                            <div className="col-md-12">
                                <div classname="container" style={{marginBottom: "5px"}}>
                                  <Link  to={`/modificaimmobile/${props.id}`} className="float-right btn btn-outline-danger" style={{}} onClick={""}>Modifica</Link>
                                </div>
                                <h4>{props.titolo}</h4>
                                <hr />
                            </div>
                            <img src="//placehold.it/500" className="col-lg-3" alt="" />
                            <p className="card-title col-lg-9">{props.descrizione}</p>
                            <div className="col-sm-12">
                            </div>
                        </div>
                        <div className="card-footer w-100 text-muted">
                            <Link to={`/sponsorizzaimmobile/${props.id}`} className="float-left btn btn-outline-danger" onClick={""}>Sponsorizza</Link>
                            <Link to="#" className="float-right btn btn-warning" style={{margin: "2px"}} onClick={""}>Elimina</Link>
                        </div>
                    </div>
                </div>
            </div>
            <br />
        </div>


    );
}


export default ImmobileUtente
