import React from "react";
import { Link } from "react-router-dom";

const ImmobileSeleziona = (props) => {

  return (
    <div>
      <div className="row justify-content-center">
        <div className="col-lg-10">
          <div className="card">
            <div className="row card-body">
              <div className="col-md-12">
                <h4>{props.titolo}</h4>
                <hr />
              </div>
              <img src="//placehold.it/500" className="col-lg-3" alt="" />
              <p className="card-title col-lg-9">{props.descrizione}</p>
              <div className="col-sm-12"></div>
            </div>
            <div className="card-footer p-2">
              <div className="row justify-content-center">
                <div className="col-4 text-center">
                  <Link
                    to="/:userId/inviadati"
                    className="btn btn-lg btn-outline-danger"
                  >
                    Seleziona
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <br />
    </div>
  );
};

export default ImmobileSeleziona;
