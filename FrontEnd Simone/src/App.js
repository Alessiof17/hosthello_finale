import React, { Component } from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
} from "react-router-dom";

//components
import NavBar from './components/shared/navbar';

//pages
import Home from "./pages/home";
import ReimpostaPsw from "./pages/recuperapassword";
import FormAccesso from "./pages/accedi";
import FormRegistrati from "./pages/registrati";
import DettagliProfilo from "./pages/gestioneprofilo";
import ModificaProfilo from "./pages/modificaprofilo";
import GestioneImmobili from "./pages/gestioneimmobili";
import FormAggiungiImmobile from "./pages/aggiungiimmobile";
import ModificaImmobile from "./pages/modificaimmobile";
import SponsorizzaImmobile from "./pages/sponsorizzaimmobile";
import Ricerca from "./pages/ricerca";
import ListaImmobbiliBur from './pages/listaimmobili-burocrazia'
import FormBurocrazia from "./pages/burocrazia";
import ListaImmobiliDati from "./pages/listaimmobili-inviodati";
import InviaDati from "./pages/inviadatinotifica";
import PaginaNonTrovata from "./components/shared/paginanontrovata"

class App extends Component {
  render() {
    return (
      <Router>
        <NavBar></NavBar>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/accedi" component={FormAccesso} />
          <Route exact path="/recuperapassword" component={ReimpostaPsw}/>
          <Route exact path="/registrati" component={FormRegistrati}/>
          <Route exact path="/ricerca" component={Ricerca}/>
          <Route path="/:userId/gestioneprofilo" component={DettagliProfilo}/>
          <Route path="/modificaprofilo/:userId" component={ModificaProfilo}/>
          <Route exact path="/:userId/gestioneimmobili" component={GestioneImmobili}/>
          <Route path="/:userId/aggiungiimmobile" component={FormAggiungiImmobile}/>
          <Route path="/modificaimmobile/:immobileId" component={ModificaImmobile}/>
          <Route path="/sponsorizzaimmobile/:immobileId" component={SponsorizzaImmobile}/>
          <Route path="/prova" component={ListaImmobbiliBur}/>
          <Route path="/inseriscidati" component={FormBurocrazia}/>
          <Route path="/:userId/notificaturismo" component={ListaImmobiliDati}/>
          <Route path="/:userId/inviadati" component={InviaDati}/>
          <Route path="/404" component={PaginaNonTrovata} /> 
          <Redirect to="/404" />
        </Switch>
        <footer className="page-footer font-small blue" style={{bottom: "0"}}>
          <div className="footer-copyright text-center py-3" style={{color: "white"}}>© 2020 Copyright: Hosthello.com
          </div>
        </footer>
      </Router>
    );
  }
}

export default App;
