import React, { useReducer, useEffect } from "react";

import { validate } from "../util/validators";

const inputReducer = (state, action) => {
  switch (action.type) {
    case "CHANGE":
      return {
        ...state,
        value: action.val,
        isValid: validate(action.val, action.validators),
      };
    case "TOUCH": {
      return {
        ...state,
        isTouched: true,
      };
    }
    default:
      return state;
  }
};

const Input = (props) => {
  const [inputState, dispatch] = useReducer(inputReducer, {
    value: props.value || "",
    isTouched: false,
    isValid: props.valid || false,
  });

  const { id, onInput } = props;
  const { value, isValid } = inputState;

  useEffect(() => {
    onInput(id, value, isValid);
  }, [id, value, isValid, onInput]);

  const changeHandler = (event) => {
    dispatch({
      type: "CHANGE",
      val: event.target.value,
      validators: props.validators,
    });
  };

  const touchHandler = () => {
    dispatch({
      type: "TOUCH",
    });
  };

  const element =
    props.element === "input" ? (
      <input
        id={props.id}
        type={props.type}
        placeholder={props.placeholder}
        onChange={changeHandler}
        onBlur={touchHandler}
        value={inputState.value}
        className={"form-control"}
        style={props.style}
        min={props.min}
        max={props.max}
      />
    ) : (
      <select
        className="custom-select"
        onChange={changeHandler}
        value={inputState.value}
      >
        <option value="" defaultValue disabled>
          Seleziona
        </option>
        <option defaultValue>B&B</option>
        <option>Casa Vacanza</option>
      </select>
    );

  return (
    <div className="form-group mb-0  row">

      {element}
      {!inputState.isValid && inputState.isTouched && <div className="col text-center"><p className="ml-auto mr-auto" style={{color:'white',  backgroundColor:'#ff8400', borderRadius:'10px', marginTop:'5px' }}>{props.errorText}</p></div>}
      
    </div>
  );
};

export default Input;
