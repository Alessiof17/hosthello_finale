import React from "react";
import ImmobileUtente from "../components/immobileutente";
import { Link } from "react-router-dom";

const ListaImmobile = (props) => {
  if (props.items.length === 0) {
    return (
      <div className="col-md-9 mx-auto mt-5">
        <div className="col-md-12">
          <div className="text-center">
            <div ClassName="title ">
              <h2>Non hai inserito alcun Immobile</h2>
              <p>Inserisci un nuovo Immobile</p>
            </div>
            <Link
              to="/:userId/aggiungiimmobile"
              style={{ textDecoration: "none" }}
            >
              <button
                className="btn btn-outline-danger rounded-circle pl-3 pr-3 d-flex justify-content-center"
                style={{ fontSize: "25px", textAlign: "center" }}
              >
                +
              </button>
            </Link>
          </div>
        </div>
      </div>
    );
  }

  return (
    <div className="col-lg-7 mx-auto">
      
      <br></br>
      <div className="d-flex justify-content-center">
        <Link
          to="/:userId/aggiungiimmobile"
          style={{
            textDecoration: "none",
            backgroundColor: "white",
            borderTopLeftRadius: "5px",
            borderTopRightRadius: "5px",
            borderBottomLeftRadius: "5px",
            borderBottomRightRadius: "5px",
          }}
        >
          <button className="btn btn-outline-danger">Aggiungi</button>
        </Link>
      </div>
      <br></br>
      {props.items.map((immobile) => (
        <ImmobileUtente
          key={immobile.id}
          id={immobile.id}
          immagine={immobile.immagineC}
          titolo={immobile.titolo}
          descrizione={immobile.descrizione}
          servizi={immobile.servizi}
          indirizzo={immobile.indirizzo}
          nomeProprietario={immobile.proprietario}
          citta={immobile.citta}
        />
      ))}
    </div>
  );
};

export default ListaImmobile;
