import React from "react";

import SearchBar from "../components/searchbar";
import BarraFiltri from "../components/barrafiltri";
import ImmobileRicerca from "../components/immobilericerca";
import { Link } from "react-router-dom";
import { useState } from "react";

const Ricerca = () => {
  const [listaImmobili, setListaImmobili] = useState([
    {
      id: "1", // fase di fetching
      immagineC: "/placehold.it/500",
      titolo: "Il mio Immobile",
      descrizione: "Breve descrizione dell'immobile",
      servizi: {
        wifi: "gratis",
        pensione: "mezzapensione",
        condizionatore: "condizionatore",
        portineria: "portineria",
        nLetti: 3,
        nStanze: 4,
        parcheggio: "parcheggio privato",
        tv: "Tv disponibile",
        animaliAmmessi: {
          cane: "cani ammessi",
          gatto: "gatti non ammessi",
        },
      },
      indirizzo: "Lungomare Cristoforo Colombo",
      citta: "Palermo",
      proprietario: "Franci",
    },
    {
      id: "2", // fase di fetching
      immagineC: "/placehold.it/500",
      titolo: "Il mio Immobile 2",
      descrizione: "Breve descrizione dell'immobile 2",
      servizi: {
        wifi: "gratis 2",
        pensione: "mezzapensione2 ",
        condizionatore: "condizionatore",
        portineria: "portineria",
        nLetti: 3,
        nStanze: 4,
        parcheggio: "parcheggio privato",
        tv: "Tv disponibile",
        animaliAmmessi: {
          cane: "cani ammessi",
          gatto: "gatti non ammessi",
        },
      },
      indirizzo: "Lungomare Cristoforo Colombo",
      citta: "Palermo",
      propietario: "Jonny",
    },
  ]);

  return (
    <div className="container">
      <div>
        <SearchBar />
      </div>
          <BarraFiltri />
     <br></br>
      {listaImmobili.map((immobile) => (
        <ImmobileRicerca
          key={immobile.id}
          id={immobile.id}
          immagine={immobile.immagineC}
          titolo={immobile.titolo}
          descrizione={immobile.descrizione}
          servizi={immobile.servizi}
          indirizzo={immobile.indirizzo}
          nomeProprietario={immobile.proprietario}
          citta={immobile.citta}
        />
      ))}
    </div>
  );
};

export default Ricerca;
