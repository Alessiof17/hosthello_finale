import React from "react";

import Sponsorizzazione from "../components/sponsorizzazione";
import InputSponsorizza from "../elementiform/input-sponsorizza";
import { useForm } from "../util/form-hook";
import { Link } from "react-router-dom";

const ListaSponsorizzazioni = (props) => {
  const [formState, inputHandler] = useForm({}, false);

  return (
    <div className="col-lg-7 mx-auto">
      <br></br>
      <div className="row justify-content-center">
        <div className="col-md-8">
          <div className="card">
            <div
              className="row card-body p-0 m-0"
              style={{ boxShadow: "0px 15px 15px rgb(112, 57, 6)" }}
            >
              <div className="col-md p-0">
                <div
                  className="card-body text-center"
                  style={{ color: " #ff8400" }}
                >
                  <h3>INSERISCI CARTA DI CREDITO</h3>

                  <InputSponsorizza
                    id=""
                    element="input"
                    type="text"
                    style={{ border: "none", marginTop: "20px" }}
                    onInput={inputHandler}
                    placeholder="0000-0000-0000-0000"
                    errorText="Inserisci un valore valido"
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <br></br>

      {props.items.map((sponsorizzazione) => (
        <Sponsorizzazione
          key={sponsorizzazione.id}
          id={sponsorizzazione.id}
          nome={sponsorizzazione.nome}
          durata={sponsorizzazione.durata}
          costo={sponsorizzazione.costo}
          handler={sponsorizzazione.handler}
        />
      ))}
    </div>
  );
};

export default ListaSponsorizzazioni;
