import React from "react";

const InviaDati = () => {
  return (
    <div className="container">
      <br />
      <div className="row mt-5 justify-content-center">
        <div className="card col-2 p-0">
          <div className="card-header">
              <h5 className="text-center">Invia Dati</h5>
          </div>
          <button className="btn btn-outline-danger">Invia</button>
        </div>
      </div>
    </div>
  );
};

export default InviaDati;
