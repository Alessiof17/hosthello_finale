import React from "react";

import Input from '../elementiform/input';
import { VALIDATOR_REQUIRE, VALIDATOR_EMAIL } from '../util/validators'
import { useForm } from "../util/form-hook";


const FormRegistrati = () => {
  const [formState, inputHandler] = useForm({}, false);


  const InputUtente =event=>{
    event.preventDefault();
    console.log(formState.inputs) //manda al backend

  }

  return (
    
    <div className="col-md-6 mx-auto">
      <br></br>
      <div className="card">
        <div className="card-body">
          <div className="row">
            <div className="col-md-12">
              <h4>Registrati</h4>
              <hr />
            </div>
          </div>
          <div className="row">
            <div className="col-md-12">
              <form onSubmit={InputUtente}>
                <Input
                  id="Nome"
                  element="input"
                  type="text"
                  label="Nome"
                  validators={[VALIDATOR_REQUIRE()]}
                  errorText="Inserisci un valore valido."
                  onInput={inputHandler}
                />
                <Input
                  id="Cognome"
                  element="input"
                  type="text"
                  label="Cognome"
                  validators={[VALIDATOR_REQUIRE()]}
                  errorText="Inserisci un valore valido."
                  onInput={inputHandler}
                />
                <Input
                  id="Data_di_nascita"
                  element="input"
                  type="date"
                  label="Data di Nascita"
                  validators={[VALIDATOR_REQUIRE()]}
                  errorText="Inserisci un valore valido."
                  onInput={inputHandler}
                />
                <Input
                  id="Città"
                  element="input"
                  type="text"
                  label="Città"
                  validators={[VALIDATOR_REQUIRE()]}
                  errorText="Inserisci un valore valido."
                  onInput={inputHandler}
                />
                <Input
                  id="Numero_telefono"
                  element="input"
                  type="text"
                  label="Numero Di Telefono"
                  validators={[VALIDATOR_REQUIRE()]}
                  errorText="Inserisci un valore valido."
                  onInput={inputHandler}
                />
                <Input
                  id="Email"
                  element="input"
                  type="text"
                  label="E-mail"
                  validators={[VALIDATOR_EMAIL()]}
                  errorText="Inserisci un valore valido."
                  onInput={inputHandler}
                />
                <Input
                  id="Password"
                  element="input"
                  type="text"
                  label="Password"
                  validators={[VALIDATOR_REQUIRE()]}
                  errorText="Inserisci un valore valido."
                  onInput={inputHandler}
                />
                <Input
                  id="Conferma_password"
                  element="input"
                  type="text"
                  label="Conferma Password"
                  validators={[VALIDATOR_REQUIRE()]}
                  errorText="Inserisci un valore valido."
                  onInput={inputHandler}
                />
                <div className="form-group float-right">
                  <button name="submit" className="btn btn-outline-danger" disabled={!formState.isValid}>Crea</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};


export default FormRegistrati