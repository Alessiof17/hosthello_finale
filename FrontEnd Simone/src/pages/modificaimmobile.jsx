import React from "react";
import { useParams } from "react-router-dom";

import Input from "../elementiform/input";
import { VALIDATOR_REQUIRE, VALIDATOR_MINLENGTH } from "../util/validators";
import { useForm } from "../util/form-hook";

const listaImmobili = [
  {
    id: "1", // fase di fetching
    immagineC: "/placehold.it/500",
    titolo: "Il mio Immobile",
    descrizione: "Breve descrizione dell'immobile",
    servizi: {
      wifi: "gratis",
      pensione: "mezzapensione",
      condizionatore: "condizionatore",
      portineria: "portineria",
      nLetti: 3,
      nStanze: 4,
      parcheggio: "parcheggio privato",
      tv: "Tv disponibile",
      animaliAmmessi: {
        cane: "cani ammessi",
        gatto: "gatti non ammessi",
      },
    },
    indirizzo: "Lungomare Cristoforo Colombo",
    citta: "Palermo",
    proprietario: "Franci",
  },
  {
    id: "2", // fase di fetching
    immagineC: "/placehold.it/500",
    titolo: "Il mio Immobile 2",
    descrizione: "Breve descrizione dell'immobile 2",
    servizi: {
      wifi: "gratis 2",
      pensione: "mezzapensione2 ",
      condizionatore: "condizionatore",
      portineria: "portineria",
      nLetti: 3,
      nStanze: 4,
      parcheggio: "parcheggio privato",
      tv: "Tv disponibile",
      animaliAmmessi: {
        cane: "cani ammessi",
        gatto: "gatti non ammessi",
      },
    },
    indirizzo: "Lungomare Cristoforo Colombo",
    citta: "Palermo",
    propietario: "Jonny",
  },
];

const ModificaImmobile = () => {
  const immobileId = useParams().immobileId;
  const identifiedImmobile = listaImmobili.find((i) => i.id === immobileId);

  const [formState, inputHandler] = useForm({}, false);

  const InputUtente = (event) => {
    event.preventDefault();
    console.log(formState.inputs);
  };

  if (!identifiedImmobile) {
    return <div>IMMOBILE NON TROVATO</div>;
  }

  return (

    <div className="col-md-6 mx-auto">
      <br></br>
      <div className="card">
        <div className="card-body">
          <div className="row">
            <div className="col-md-12">
              <h4>Modifica Immobile</h4>
              <hr />
            </div>
          </div>
          <div className="row">
            <div className="col-md-12">
            <form onSubmit={InputUtente}>
              <Input
                id="titolo"
                element="input"
                type="text"
                label="Titolo"
                validators={[VALIDATOR_REQUIRE()]}
                errorText="Inserire un valore valido."
                onInput={inputHandler}
                value={identifiedImmobile.titolo}
                valid={true}
              />
              <Input
                id="descrizione"
                element="textarea"
                label="Descrizione"
                validators={[VALIDATOR_REQUIRE()]}
                errorText="Inserire un valore valido."
                onInput={inputHandler}
                value={identifiedImmobile.descrizione}
                valid={true}
              />
              <div className="form-group float-right">
                <button type="submit" className="btn btn-outline-danger" disabled={inputHandler}>
                  AGGIORNA
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    </div>
  );
};

export default ModificaImmobile;
