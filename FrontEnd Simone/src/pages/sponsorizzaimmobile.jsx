import React from "react";

import ListaSponsorizzazioni from "./listasponsorizzazioni";

const listaSponsorizzazioni = [
  {
    id: "1", // fase di fetching
    nome: "PIANO SPONSORIZZAZIONE 1",
    durata: "1 Mese",
    costo: "4.99",
    handler: false,
  },
  {
    id: "2", // fase di fetching
    nome: "PIANO SPONSORIZZAZIONE 2",
    durata: "3 Mesi",
    costo: "12.99",
    handler: false,
  },
  {
    id: "3", // fase di fetching
    nome: "PIANO SPONSORIZZAZIONE 3",
    durata: "12 Mesi",
    costo: "49.99",
    handler: false,
  },
];

const SponsorizzaImmobile = () => {

  return (
    <div>
      <ListaSponsorizzazioni items={listaSponsorizzazioni} />
    </div>
  );
};

export default SponsorizzaImmobile;
