import React from "react";


import ImmobileBurocrazia from "../components/immobile-burocrazia";

const ListaImmobiliBur = () => {

const listaImmobili = [{
    id: "1", // fase di fetching
    immagineC: "/placehold.it/500",
    titolo: "Il mio Immobile",
    descrizione: "Breve descrizione dell'immobile",
    servizi: {
      wifi: "gratis",
      pensione: "mezzapensione",
      condizionatore: "condizionatore",
      portineria: "portineria",
      nLetti: 3,
      nStanze: 4,
      parcheggio: "parcheggio privato",
      tv: "Tv disponibile",
      animaliAmmessi: {
        cane: "cani ammessi",
        gatto: "gatti non ammessi",
      },
    },
    indirizzo: "Lungomare Cristoforo Colombo",
    citta: "Palermo",
    proprietario: "Franci",
  },
  {
    id: "2", // fase di fetching
    immagineC: "/placehold.it/500",
    titolo: "Il mio Immobile 2",
    descrizione: "Breve descrizione dell'immobile 2",
    servizi: {
      wifi: "gratis 2",
      pensione: "mezzapensione2 ",
      condizionatore: "condizionatore",
      portineria: "portineria",
      nLetti: 3,
      nStanze: 4,
      parcheggio: "parcheggio privato",
      tv: "Tv disponibile",
      animaliAmmessi: {
        cane: "cani ammessi",
        gatto: "gatti non ammessi",
      },
    },
    indirizzo: "Lungomare Cristoforo Colombo",
    citta: "Palermo",
    propietario: "Jonny",
  },
];

  return ( <div className="col-lg-7 mx-auto">
  <br></br>
  {listaImmobili.map((immobile) => (
    <ImmobileBurocrazia
      key={immobile.id}
      id={immobile.id}
      immagine={immobile.immagineC}
      titolo={immobile.titolo}
      descrizione={immobile.descrizione}
      servizi={immobile.servizi}
      indirizzo={immobile.indirizzo}
      nomeProprietario={immobile.proprietario}
      citta={immobile.citta}
    />
  ))}
</div>
   
  );
};

export default ListaImmobiliBur;

/*
 <div className="container">
      <br></br>
      <div className="row justify-content-center">
        <div className="col-md-9">
          <div className="card">
            <div className="card-body">
              <div className="row">
                <div className="col-md-12">
                  <h4>Il Tuo Profilo</h4>
                  <hr />
                </div>
              </div>
              <div className="row">
                <div className="col-md-12">
                  <form onSubmit={InputUtente}>
                    <Input
                      id="nome_persona"
                      element="input"
                      type="text"
                      label="Nome"
                      validators={[]}
                      onInput={inputHandler}
                      placeholder={"Nome"}
                    />
                    <Input
                      id="cognome_persona"
                      element="input"
                      type="text"
                      label="Cognome"
                      validators={[]}
                      onInput={inputHandler}
                      placeholder={"Cognome"}
                    />

                    <Input
                      id="data_nascita_persona"
                      element="input"
                      type="date"
                      label="Data di nascita"
                      validators={[VALIDATOR_REQUIRE]}
                      onInput={inputHandler}
                      placeholder={""}
                    />

                    <Input
                      id="citta_residenza"
                      element="input"
                      type="text"
                      label="Città"
                      validators={[VALIDATOR_REQUIRE]}
                      onInput={inputHandler}
                      placeholder={"Città"}
                    />
                    <Input
                      id="provincia_residenza"
                      element="input"
                      type="text"
                      label="Provincia"
                      validators={[VALIDATOR_REQUIRE]}
                      onInput={inputHandler}
                      placeholder={"Provincia"}
                    />

                    <div className="form-group row">
                      <div className="offset-4 col">
                        <button
                          name="submit"
                          className="btn btn-outline-danger"
                          disabled={!formState.isValid}
                        >
                          Aggiungi
                        </button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

*/