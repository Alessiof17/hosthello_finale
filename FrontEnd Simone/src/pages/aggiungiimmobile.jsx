import React from "react";

import Input from "../elementiform/input";
import InputStruttura from "../elementiform/input-struttra-aggiungiimmobile";
import { VALIDATOR_REQUIRE } from "../util/validators";
import { useForm } from "../util/form-hook";

const FormAggiungiImmobile = () => {
  const [formState, inputHandler] = useForm({}, false);

  const InputUtente = (event) => {
    event.preventDefault();
    console.log(formState.inputs); //manda al backend
  };

  return (
    <div className="col-md-6 mx-auto">
      <br></br>
      <div className="card">
        <div className="card-body">
          <div className="row">
            <div className="col-md-12">
              <h4>Aggiungi Immobile</h4>
              <hr />
            </div>
          </div>
          <div className="row">
            <div className="col-md-12">
              <form onSubmit={InputUtente}>
                <Input
                  id="nome_immobile"
                  element="input"
                  type="text"
                  label="Titolo Annuncio"
                  validators={[VALIDATOR_REQUIRE()]}
                  errorText="Inserisci un valore valido."
                  onInput={inputHandler}
                />
                <Input
                  id="citta_immobile"
                  element="input"
                  type="text"
                  label="Città"
                  validators={[VALIDATOR_REQUIRE()]}
                  errorText="Inserisci un valore valido."
                  onInput={inputHandler}
                />
                <Input
                  id="provincia_immobile"
                  element="input"
                  type="text"
                  label="Provincia"
                  validators={[VALIDATOR_REQUIRE()]}
                  errorText="Inserisci un valore valido."
                  onInput={inputHandler}
                />
                <Input
                  id="regione_immobile"
                  element="input"
                  type="text"
                  label="Regione"
                  validators={[VALIDATOR_REQUIRE()]}
                  errorText="Inserisci un valore valido."
                  onInput={inputHandler}
                />
                <Input
                  id="indirizzo_immobile"
                  element="input"
                  type="text"
                  label="Indirizzo"
                  validators={[VALIDATOR_REQUIRE()]}
                  errorText="Inserisci un valore valido."
                  onInput={inputHandler}
                />
                <Input
                  id="costo"
                  element="input"
                  type="number"
                  label="Costo Per Notte"
                  validators={[VALIDATOR_REQUIRE()]}
                  errorText="Inserisci un valore valido."
                  onInput={inputHandler}
                />
                <Input
                  id="numero_stanze"
                  element="input"
                  type="number"
                  label="Numero Stanze"
                  validators={[VALIDATOR_REQUIRE()]}
                  errorText="Inserisci un valore valido."
                  onInput={inputHandler}
                />
                <Input
                  id="posti_letto"
                  element="input"
                  type="number"
                  label="Posti Letto"
                  validators={[VALIDATOR_REQUIRE()]}
                  errorText="Inserisci un valore valido."
                  onInput={inputHandler}
                />
                <Input
                  id="numero_bagni"
                  element="input"
                  type="number"
                  label="Numero Bagni"
                  validators={[VALIDATOR_REQUIRE]}
                  onInput={inputHandler}
                />
                <Input
                  id="descrizione"
                  element="textarea"
                  label="Descrizione"
                  validators={[VALIDATOR_REQUIRE()]}
                  errorText="Inserisci un valore valido."
                  placeholder="Aggiungi una breve descrizione"
                  onInput={inputHandler}
                />
                <InputStruttura
                  id="tipo_struttura"
                  element="select"
                  label="Tipo Struttura"
                  type="text"
                  validators={[VALIDATOR_REQUIRE()]}
                  errorText="Seleziona un valore valido"
                  onInput={inputHandler}
                />
                <Input
                  id="punti_interesse"
                  element="textarea"
                  label="Punti Di Interesse"
                  valid={true}
                  placeholder="Aggiungi una breve lista dei posti turistici di interesse"
                  onInput={inputHandler}
                  />
                <div className="form-group float-right">
                  <button
                    name="submit"
                    className="btn btn-outline-danger"
                    disabled={!formState.isValid}
                  >
                    Crea
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FormAggiungiImmobile;
