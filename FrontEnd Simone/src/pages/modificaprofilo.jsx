import React from "react";
import { Link } from 'react-router-dom';
import{useParams } from 'react-router-dom'


import Input from '../elementiform/input';
import { VALIDATOR_REQUIRE } from '../util/validators'
import { useForm } from "../util/form-hook";

const ARRAY_USER = [
  {
    id: "1",
    username: "tony",
    nome: "tony",
    cognome: "capelli",
    email: "tony@gmail.com",
    data_nascita: "1998/07/29",
    citta: "Palermo",
   telefono: "3661744807",
    descrizione: "prova",
  },
  {
    id: "2",
    username: "tony",
    nome: "tony",
    cognome: "capelli",
    email: "tony@gmail.com",
    data_nascita: "1998/07/29",
    citta: "Palermo",
    telefono: "3661744807",
    descrizione: "prova",
  },
];

const ModificaProfilo = () => {

  const userId = useParams().userId;
  const identifiedPlace = ARRAY_USER.find(p => p.id === userId);

  const[formState, inputHandler ]= useForm({}, false);

  const InputUtente = event => {
    event.preventDefault();
    console.log(formState.inputs)

  }

  return (
    <div className="container">
      <br></br>
      <div className="row align-items-start">

        <div className="col-md-3 ">
          <div className="list-group ">
          <Link to="/:userId/gestioneprofilo" style={{ textDecoration: 'none', borderTopLeftRadius: "10px", borderTopRightRadius: "10px" }}><button className="btn btn-outline-danger list-group-item list-group-item-action">Dettagli Profilo</button></Link>
          <Link to="/cronologiaguadagni" style={{ textDecoration: 'none', borderBottomLeftRadius: "10px", borderBottomRightRadius: "10px" }}><button className="btn btn-outline-danger list-group-item list-group-item-action">Cronologia guadagni</button></Link>
          </div>
        </div>

        <div className="col-md-9">
          <div className="card">
            <div className="card-body">
              <div className="row">
                <div className="col-md-12">
                  <h4>Modifica Il Tuo Profilo</h4>
                  <hr />
                </div>
              </div>
              <div className="row">
                <div className="col-md-12">
                  <form onSubmit={InputUtente} >
                    <Input
                      id="username"
                      element="input"
                      type="text"
                      label="User Name"
                      validators={[VALIDATOR_REQUIRE()]}
                      errorText="Inserisci il tuo username."
                      onInput={inputHandler}
                      placeholder={"User Name"}
                      value={identifiedPlace.username}
                      valid={true}
                    />
                    <Input
                      id="nome"
                      element="input"
                      type="text"
                      label="Nome"
                      validators={[]}
                      onInput={inputHandler}
                      placeholder={"Nome"}
                      value={identifiedPlace.nome}
                      valid={true}
                    />
                    <Input
                      id="cognome"
                      element="input"
                      type="text"
                      label="Cognome"
                      validators={[]}
                      onInput={inputHandler}
                      placeholder={"Cognome"}
                      value={identifiedPlace.cognome}
                      valid={true}
                    />

                    <Input
                      id="email"
                      element="input"
                      type="email"
                      label="E-mail"
                      validators={[]}
                      onInput={inputHandler}
                      placeholder={"e-mail"}
                      value={identifiedPlace.email}
                      valid={true}
                    />


                    <Input
                      id="data_di_nascita"
                      element="input"
                      type="date"
                      label="Data di nascita"
                      validators={[VALIDATOR_REQUIRE]}
                      onInput={inputHandler}
                      placeholder={""}
                      value={identifiedPlace.data_nascita}
                      valid={true}
                    />


                    <Input
                      id="citta"
                      element="input"
                      type="text"
                      label="Città"
                      validators={[VALIDATOR_REQUIRE]}
                      onInput={inputHandler}
                      placeholder={"Città"}
                      value={identifiedPlace.citta}
                      valid={true}
                    />

                    <Input
                      id="num_telefono"
                      element="input"
                      type="text"
                      label="Numero di telefono"
                      validators={[]}
                      onInput={inputHandler}
                      placeholder={"+39-3---------"}
                      value={identifiedPlace.telefono}
                      valid={true}
                    />

                    <Input
                      id="descrizione"
                      element="textarea"
                      type="text"
                      label="Descrizione"
                      validators={[]}
                      onInput={inputHandler}
                      placeholder={""}
                      value={identifiedPlace.descrizione}
                      valid={true}
                    />

                    <Input
                      id="password"
                      element="input"
                      type="password"
                      label="Password*"
                      validators={[VALIDATOR_REQUIRE]}
                      onInput={inputHandler}
                    />
                    <div className="form-group row">
                      <div className="offset-4 col-8">
                        <button name="submit" className="btn btn-outline-danger btn-lg" disabled={!formState.isValid}>Aggiorna Profilo</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>

  );
}


export default ModificaProfilo