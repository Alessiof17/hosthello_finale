import React from "react";

import SearchBar from "../components/searchbar";


const Home = () => {
  return (
    <div className="home">
      <div className="container">
        <div className="min-vh-100 d-flex flex-column justify-content-center">
          <div className="text-center">
            <img
              src={"https://i.ibb.co/5vt2zyL/brand-logo.png"}
              className="img-fluid"
              alt="HostHello"
            />
          </div>
          <div>
            <SearchBar />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Home;
