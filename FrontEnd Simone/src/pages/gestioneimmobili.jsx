import React, { useState } from 'react'

import ListaImmobili from './listaimmobili';

const listaImmobili = [
    {
      id: "1", // fase di fetching
      immagineC: "/placehold.it/500",
      titolo: "Il mio Immobile",
      descrizione: "Breve descrizione dell'immobile",
      servizi: {
        wifi: "gratis",
        pensione: "mezzapensione",
        condizionatore: "condizionatore",
        portineria: "portineria",
        nLetti: 3,
        nStanze: 4,
        parcheggio: "parcheggio privato",
        tv: "Tv disponibile",
        animaliAmmessi: {
          cane: "cani ammessi",
          gatto: "gatti non ammessi",
        },
      },
      indirizzo: "Lungomare Cristoforo Colombo",
      citta: "Palermo",
      proprietario: "Franci",
    },
    {
      id: "2", // fase di fetching
      immagineC: "/placehold.it/500",
      titolo: "Il mio Immobile 2",
      descrizione: "Breve descrizione dell'immobile 2",
      servizi: {
        wifi: "gratis 2",
        pensione: "mezzapensione2 ",
        condizionatore: "condizionatore",
        portineria: "portineria",
        nLetti: 3,
        nStanze: 4,
        parcheggio: "parcheggio privato",
        tv: "Tv disponibile",
        animaliAmmessi: {
          cane: "cani ammessi",
          gatto: "gatti non ammessi",
        },
      },
      indirizzo: "Lungomare Cristoforo Colombo",
      citta: "Palermo",
      propietario: "Jonny",
    },
];

const GestioneImmobili = () => {
    //const userId = useParams().userId;
    //const immobiliTrovati = listaImmobili.filter(immobile => immobile.proprietario === userId);
    //return <ListaImmobili items={immobiliTrovati}

    return <ListaImmobili items={listaImmobili} />;

};

export default GestioneImmobili;