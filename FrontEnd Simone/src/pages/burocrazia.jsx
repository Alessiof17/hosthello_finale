import React from "react";
import Input from "../elementiform/input";
import { VALIDATOR_REQUIRE } from "../util/validators";
import { useForm } from "../util/form-hook";

const FormBurocrazia = props => {
  const [formState, inputHandler] = useForm({}, false);

  const InputUtente = (event) => {
    event.preventDefault();
    console.log(formState.inputs);
  };

  return (
    <div className="container">
      <br></br>
      <div className="row justify-content-center">
        <div className="col-md-9">
          <div className="card">
            <div className="card-body">
              <div className="row">
                <div className="col-md-12">
                  <h4>Inserisci dati</h4>
                  <hr />
                </div>
              </div>
              <div className="row">
                <div className="col-md-12">
                  <form onSubmit={InputUtente}>
                    <Input
                      id="nome_persona"
                      element="input"
                      type="text"
                      label="Nome"
                      validators={[VALIDATOR_REQUIRE]}
                      onInput={inputHandler}
                      placeholder={"Nome"}
                    />
                    <Input
                      id="cognome_persona"
                      element="input"
                      type="text"
                      label="Cognome"
                      validators={[VALIDATOR_REQUIRE]}
                      onInput={inputHandler}
                      placeholder={"Cognome"}
                    />

                    <Input
                      id="data_nascita_persona"
                      element="input"
                      type="date"
                      label="Data di nascita"
                      validators={[VALIDATOR_REQUIRE]}
                      onInput={inputHandler}
                      placeholder={""}
                    />

                    <Input
                      id="citta_residenza"
                      element="input"
                      type="text"
                      label="Città"
                      validators={[VALIDATOR_REQUIRE]}
                      onInput={inputHandler}
                      placeholder={"Città"}
                    />
                    <Input
                      id="provincia_residenza"
                      element="input"
                      type="text"
                      label="Provincia"
                      validators={[VALIDATOR_REQUIRE]}
                      onInput={inputHandler}
                      placeholder={"Provincia"}
                    />

                    <div className="form-group row">
                      <div className="offset-4 col">
                        <button
                          name="submit"
                          className="btn btn-outline-danger"
                          disabled={!formState.isValid}
                        >
                          Aggiungi
                        </button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FormBurocrazia;
