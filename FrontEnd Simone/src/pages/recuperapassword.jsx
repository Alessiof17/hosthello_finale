import React from "react"

const ReimpostaPsw = () => {
  return (
    <div className="container">
      <div className="row">
        <div className="col-md-12 min-vh-100 d-flex flex-column justify-content-center">
          <div className="row">
            <div className="col-lg-6 col-md-8 mx-auto">
              <div className="card rounded-10 shadow shadow-sm">
                <div className="card-header">
                  <h3 className="mb-0 text-center">Recupera Password</h3>
                </div>
                <div className="card-body">
                  <form className="register-form" id="formLogin" method="POST">
                    <div className="form-group">
                      <label >E-mail</label>
                      <input id="email" name="email" className="form-control form-control-lg" type="email" required />
                      <div className="invalid-feedback">Email non valida!</div>
                    </div>

                    <div className="radio">
                      <label><input type="radio" name="optradio" required /> Il tuo colore preferito?</label>
                    </div>
                    <div className="radio">
                      <label><input type="radio" name="optradio" required /> La tua città dei sogni?</label>
                    </div>
                    <div className="radio">
                      <label><input type="radio" name="optradio" required /> Il nome del tuo primo animale domestico</label>
                    </div>
                    <p></p>

                    <div className="form-group">
                      <label>Risposta</label>
                      <input type="text" className="form-control form-control-lg rounded-0" id="risposta" required />
                      <div className="invalid-feedback">Risposta non valida!</div>
                    </div>
                    <div>
                      <label>  </label>
                    </div>
                    <button type="submit" className="btn btn-block btn-outline-danger btn-lg" id="btninvio">Invia</button>

                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  );
}


export default ReimpostaPsw