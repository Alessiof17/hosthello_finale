import React, { useState, useContext } from "react";

import Input from "../elementiform/input-accedi";
import { VALIDATOR_REQUIRE } from "../util/validators";
import { useForm } from "../util/form-hook";
import { AuthContext } from "../context/auth-context";

const FormAccesso = () => {
  const auth = useContext(AuthContext);

  const [formState, inputHandler] = useForm({}, false);

  const InputUtente = (event) => {
    event.preventDefault();
    console.log(formState.inputs);
    auth.login();
  };

  return (
    <div className="container">
      <div className="row">
        <div className="col-md-12 min-vh-100 d-flex flex-column justify-content-center">
          <div className="row">
            <div className="col-lg-5 col-md-8 mx-auto">
              <div className="card rounded-10 shadow shadow-sm">
                <div className="card-header">
                  <h3 className="mb-0 text-center">Accedi</h3>
                </div>
                <div className="card-body">
                  <form onSubmit={InputUtente} className="form" method="POST">
                    <Input
                      id="email"
                      element="input"
                      type="email"
                      label="E-mail"
                      validators={[VALIDATOR_REQUIRE]}
                      errorText="email non valida"
                      onInput={inputHandler}
                    />
                    <Input
                      id="password"
                      element="input"
                      type="password"
                      label="Password"
                      validators={[]}
                      textError=""
                      onInput={inputHandler}
                    />
                      <br/>
                    <button
                      type="submit"
                      className="btn btn-block btn-outline-danger btn-lg"
                      id="btnLogin"
                    >
                      Accedi
                    </button>
                    <p></p>
                    <div className="text-center">
                      <a href="/recuperapassword">Recupera Password</a>
                    </div>
                    <p></p>
                    <div className="text-center">
                      <p>
                        {" "}
                        Non sei ancora registrato?
                        <a href="/registrati"> Clicca qui</a>
                      </p>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FormAccesso;

/*

                    <div>
                      <div className="custom-control custom-checkbox mb-3">
                      <Input
                      id="ricordami"
                      element="checkbox"
                      type="checkbox"
                      validators={[]}
                      onInput={inputHandler}
                    />
                        <label className="custom-control-label" for="customCheck1">
                          Ricordami
                        </label>
                      </div>
                    </div>

                    */
