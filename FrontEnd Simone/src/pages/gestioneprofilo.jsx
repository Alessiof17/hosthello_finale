import React from "react";

import Profilo from "../components/profilo";

const items = [
  {
    id: "1",
    username: "tony",
    nome: "tony",
    cognome: "capelli",
    email: "tony@gmail.com",
    data_nascita: "1998/07/29",
    citta: "Palermo",
    telefono: "3661744807",
    descrizione: "prova",
  },
];

const DettagliProfilo = () => {

  return (
    <div>
      {items.map(user => (
       <Profilo
          key={user.id}
          id={user.id}
          username={user.username}
          nome={user.nome}
          cognome={user.cognome}
          email={user.email}
          telefono={user.telefono}
          citta={user.citta}
          data_nascita={user.data_nascita}
        />
      ))}
    </div>
  );
}


export default DettagliProfilo
