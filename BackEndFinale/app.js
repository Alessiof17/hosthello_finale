const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const db = require('./config/database');

db.authenticate()
  .then(() => console.log('Database connected'))
  .catch(err => console.log('Errore'+ err))

const gestioneImmobiliRoutes= require('./routes/gestione-immobili-routes');
const dettagliProfiloRoutes = require('./routes/dettagli-profilo-routes');
const ricercaRoutes = require('./routes/ricerca-routes');
const burocraziaRoutes = require('./routes/burocrazia-routes');
const registrazioneRoutes = require('./routes/registrazione-routes');
const HttpError = require('./models/http-error');


const corsOptions = {
  origin: "http://localhost:5001"
};

const app = express();


app.use(bodyParser.json());

app.use(cors(corsOptions));
app.get('/favico.ico', (req, res) => {
  res.sendStatus(404);
});

app.use((req, res, next) => {       //Capiremo perchè quando collegheremo al front end
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept, Authorization'
  );
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PATCH, DELETE');

  next();
});


app.use('/areariservata/gestioneimmobili', gestioneImmobiliRoutes);

app.use('/areariservata/dettagliprofilo', dettagliProfiloRoutes);

app.use('/ricerca', ricercaRoutes);

app.use('/burocrazia', burocraziaRoutes);

app.use('/registrati', registrazioneRoutes)

app.use((req, res, next) => {
  const error = new HttpError('Could not find this route.', 404);
  throw error;
}); 
  
app.use((error, req, res, next) => {
  if (res.headerSent) {
    return next(error);
  }
  res.status(error.code || 500)
  res.json({message: error.message || 'An unknown error occurred!'});
}); 


app.listen(5000);