CREATE DATABASE  IF NOT EXISTS `hosthello` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `hosthello`;
-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: localhost    Database: hosthello
-- ------------------------------------------------------
-- Server version	5.7.30-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `composta`
--

DROP TABLE IF EXISTS `composta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `composta` (
  `ref_immobile` varchar(100) NOT NULL,
  `ref_servizio` int(11) NOT NULL,
  `createdat` datetime DEFAULT NULL,
  `updatedat` datetime DEFAULT NULL,
  PRIMARY KEY (`ref_servizio`,`ref_immobile`),
  KEY `composta_fk` (`ref_immobile`),
  CONSTRAINT `composta_fk` FOREIGN KEY (`ref_immobile`) REFERENCES `immobile` (`id_immobile`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `composta_fk_1` FOREIGN KEY (`ref_servizio`) REFERENCES `servizi` (`id_servizio`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `composta`
--

LOCK TABLES `composta` WRITE;
/*!40000 ALTER TABLE `composta` DISABLE KEYS */;
/*!40000 ALTER TABLE `composta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `immobile`
--

DROP TABLE IF EXISTS `immobile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `immobile` (
  `id_immobile` varchar(100) NOT NULL,
  `città_immobile` varchar(100) NOT NULL,
  `provincia_immobile` varchar(100) NOT NULL,
  `regione_immobile` varchar(100) NOT NULL,
  `indirizzo_immobile` varchar(100) NOT NULL,
  `costo_notte` float NOT NULL,
  `numero_stanze` int(11) NOT NULL,
  `posti_letto` int(11) NOT NULL,
  `numero_bagni` int(11) NOT NULL,
  `ref_proprietario` varchar(100) NOT NULL,
  `createdat` datetime DEFAULT NULL,
  `updatedat` datetime DEFAULT NULL,
  `data_inizio_spons` date DEFAULT NULL,
  `data_fine_spons` date DEFAULT NULL,
  `descrizione` varchar(1000) NOT NULL,
  `punti_interesse` varchar(1000) NOT NULL,
  PRIMARY KEY (`id_immobile`),
  KEY `immobile_fk` (`ref_proprietario`),
  CONSTRAINT `immobile_fk` FOREIGN KEY (`ref_proprietario`) REFERENCES `utente` (`id_utente`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `immobile`
--

LOCK TABLES `immobile` WRITE;
/*!40000 ALTER TABLE `immobile` DISABLE KEYS */;
INSERT INTO `immobile` VALUES ('immobile_1','Napoli','NA','Campania','via renzi',30,3,4,1,'alessio_ferrara',NULL,'2020-06-13 12:05:47',NULL,NULL,'','');
/*!40000 ALTER TABLE `immobile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `informazioni`
--

DROP TABLE IF EXISTS `informazioni`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `informazioni` (
  `id_info` int(11) NOT NULL AUTO_INCREMENT,
  `nome_persona` varchar(100) NOT NULL,
  `cognome_persona` varchar(100) NOT NULL,
  `data_nascita_persona` date NOT NULL,
  `città_residenza` varchar(100) NOT NULL,
  `provincia_residenza` varchar(100) NOT NULL,
  `indirizzo_residenza` varchar(100) NOT NULL,
  `createdat` datetime DEFAULT NULL,
  `updatedat` datetime DEFAULT NULL,
  `ref_prenotazione` varchar(100) NOT NULL,
  PRIMARY KEY (`id_info`),
  KEY `informazioni_fk` (`ref_prenotazione`),
  CONSTRAINT `informazioni_fk` FOREIGN KEY (`ref_prenotazione`) REFERENCES `prenotazione` (`id_prenotazione`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `informazioni`
--

LOCK TABLES `informazioni` WRITE;
/*!40000 ALTER TABLE `informazioni` DISABLE KEYS */;
INSERT INTO `informazioni` VALUES (1,'Veronica','Tropea','1995-04-05','Catania','CT','Via fortino pisano',NULL,NULL,'prenotazione_1');
/*!40000 ALTER TABLE `informazioni` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_datiturismo`
--

DROP TABLE IF EXISTS `log_datiturismo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `log_datiturismo` (
  `data_invio` date NOT NULL,
  `ref_immobile` varchar(100) NOT NULL,
  KEY `log_datiturismo_fk` (`ref_immobile`),
  CONSTRAINT `log_datiturismo_fk` FOREIGN KEY (`ref_immobile`) REFERENCES `immobile` (`id_immobile`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_datiturismo`
--

LOCK TABLES `log_datiturismo` WRITE;
/*!40000 ALTER TABLE `log_datiturismo` DISABLE KEYS */;
/*!40000 ALTER TABLE `log_datiturismo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prenotazione`
--

DROP TABLE IF EXISTS `prenotazione`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `prenotazione` (
  `id_prenotazione` varchar(100) NOT NULL,
  `ref_utente_prenotazione` varchar(100) DEFAULT NULL,
  `ref_immobile` varchar(100) DEFAULT NULL,
  `data_inizio` date NOT NULL,
  `data_fine` date NOT NULL,
  `costo_totale` float NOT NULL,
  `createdat` datetime DEFAULT NULL,
  `updatedat` datetime DEFAULT NULL,
  `numero_ospiti` int(11) NOT NULL,
  `numero_giorni` int(11) NOT NULL,
  `accettata` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_prenotazione`),
  KEY `prenotazione_fk_2` (`ref_immobile`),
  KEY `prenotazione_fk_3` (`ref_utente_prenotazione`),
  CONSTRAINT `prenotazione_fk` FOREIGN KEY (`ref_immobile`) REFERENCES `immobile` (`id_immobile`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `prenotazione_fk_1` FOREIGN KEY (`ref_utente_prenotazione`) REFERENCES `utente` (`id_utente`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prenotazione`
--

LOCK TABLES `prenotazione` WRITE;
/*!40000 ALTER TABLE `prenotazione` DISABLE KEYS */;
INSERT INTO `prenotazione` VALUES ('prenotazione_1','alessio_ferrara','immobile_1','2020-03-05','2020-03-09',150,NULL,NULL,0,0,0);
/*!40000 ALTER TABLE `prenotazione` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `servizi`
--

DROP TABLE IF EXISTS `servizi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `servizi` (
  `id_servizio` int(11) NOT NULL AUTO_INCREMENT,
  `descrizione` varchar(100) NOT NULL,
  `createdat` datetime DEFAULT NULL,
  `updatedat` datetime DEFAULT NULL,
  PRIMARY KEY (`id_servizio`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `servizi`
--

LOCK TABLES `servizi` WRITE;
/*!40000 ALTER TABLE `servizi` DISABLE KEYS */;
/*!40000 ALTER TABLE `servizi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `utente`
--

DROP TABLE IF EXISTS `utente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `utente` (
  `id_utente` varchar(100) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `cognome` varchar(100) NOT NULL,
  `data_nascita` date NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `telefono` varchar(100) NOT NULL,
  `domandasicurezza` varchar(100) NOT NULL,
  `rispostadomandas` varchar(100) NOT NULL,
  `createdat` datetime DEFAULT NULL,
  `updatedat` datetime DEFAULT NULL,
  PRIMARY KEY (`id_utente`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `utente`
--

LOCK TABLES `utente` WRITE;
/*!40000 ALTER TABLE `utente` DISABLE KEYS */;
INSERT INTO `utente` VALUES ('alessio_ferrara','Alessio','Ferrara','1999-04-17','alessio.ferrara17@gmail.com','Palumbo199','3425632026','Nome di tua madre?','Rosalinda',NULL,NULL);
/*!40000 ALTER TABLE `utente` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-06-14 15:01:16
