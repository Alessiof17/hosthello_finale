const HttpError = require('../models/http-error');
const { validationResult } = require('express-validator');
const Immobile = require("../models/immobile");
const Composta = require('../models/composta');
const {v4: uuid} = require("uuid");
 
const getImmobiliByIdCliente = async (req, res, next) => {
 
  const idUtente = req.userData.userId;
  let immobili;
 
  try{
      immobili = await Immobile.ottieniImmobileById(idUtente);
  }catch(err){
      return next(err);
  }
  
  if(immobili.length){
    immobili.forEach(immobile => {//vedo se sono sponsorizzati
      if(new Date(immobile.data_inizio_spons) <= data_odierna && data_odierna <= new Date(immobile.data_fine_spons)) {
          immobile.isSponsorizzata = true;
      }   
      else {
          immobile.isSponsorizzata = false;
      }
  })
  res.json({immobili});
}
  else {
    return next(
      new HttpError('Non ho trovato nessun immobile per questo utente', 404)
    );
  }
}
 
const patchImmobileById = async (req, res, next) => {
  const errors = validationResult(req)
 
  if(!errors.isEmpty()) {
    return next(new HttpError('Compilare tutti i campi obbligatori', 422));
  }
 
  let arrayServizi = [];
 
  for (let i = 0; i < (Object.keys(req.body).length - 11); i++){
    arrayServizi[i] = Object.values(req.body)[i + 11]
  }
 
  const {nome_immobile, citta, provincia, regione, indirizzo, costo, numero_stanze, posti_letto, numero_bagni, descrizione, punti_interesse} = req.body;
  const idImmobile = req.params.immobileId;
  const idUtente = req.userData.userId;

  if(Immobile.ottieniProprietario(idImmobile) !== idUtente){
    return next(new HttpError('Impossibile modificare immobile non posseduto'), 404);
  }
 
  try{
    Composta.rimuoviServizi(idImmobile);
    Immobile.modificaImmobile(idImmobile, nome_immobile, citta, provincia, regione, indirizzo, costo, numero_stanze, posti_letto, numero_bagni, descrizione, punti_interesse);
    for(let i = 0; i < arrayServizi.length; i++){
      Composta.inserisciServizio(idImmobile, array[i]);
    }
  }catch(err){
    return next(err);
  }
 
  res.status(200).json('Immobile modificato con successo');
}

const creaImmobile = async (req, res, next) => {
  const errors = validationResult(req)
  
  if(!errors.isEmpty()){
    return next(new HttpError('Compilare tutti i campi obbligatori'), 404);
  }
  const idUtente = req.userData.userId;
  let arrayServizi = [];
 
  for (let i = 0; i < (Object.keys(req.body).length - 12); i++){
    arrayServizi[i] = Object.values(req.body)[i + 12]
  }
  
  let {nome_immobile, citta, provincia, regione, indirizzo, costo, numero_stanze, posti_letto, numero_bagni, descrizione, tipo_struttura, punti_interesse} = req.body;
  
  const idImmobile = uuid();
 
  try{
      Immobile.creaImmobile(idImmobile, idUtente, nome_immobile.toLowerCase(), citta.toLowerCase(), provincia.toLowerCase(), regione.toLowerCase(), indirizzo.toLowerCase(), costo, numero_stanze, posti_letto, numero_bagni, descrizione.toLowerCase(), tipo_struttura.toLowerCase(), punti_interesse.toLowerCase());
      if(arrayServizi.length){//controllo se ci sono servizi da inserire
        for(let i = 0; i < array.length; i++){
          Composta.inserisciServizio(idImmobile, array[i]);
        }
      }
  }catch(err){
      return next(err);
  }
  
  res.status(201).json('Immobile creato con successo');
}
 
const sponsorizzaImmobileById = async (req, res, next) =>{
 
  const ONEMONTH = 2629800000;
  const THREEMONTH = 7889400000;
  const ONEYEAR = 31557600000;
  
  const idImmobile = req.params.immobileId;
  const idUtente = req.userData.userId;

  if(Immobile.ottieniProprietario(idImmobile) !== idUtente){
    return next(new HttpError('Impossibile sponsorizzare immobile non posseduto'), 404);
  }

  const {tipologia} = req.body; //tipologia un numero che può essere 1, 3, 12 
  let dataFine;
  const dataOdierna = new Date();
  
  if(tipologia === 1){dataFine = new Date(dataOdierna.getTime() + ONEMONTH)}
  if(tipologia === 3){dataFine = new Date(dataOdierna.getTime() + THREEMONTH)}
  if(tipologia === 12){dataFine = new Date(dataOdierna.getTime() + ONEYEAR)}
 
  try{
      Immobile.sponsorizzaImmobile(idImmobile, dataOdierna, dataFine);
  }catch(err){
      return next(err);
  }
 
  res.status(200).json('Immobile sponsorizzato con successo!');
}
 
const eliminaImmobile = (req, res, next) => {
 
  const idImmobile = req.params.immobileId;
  const idUtente = req.userData.userId;
  
  if(Immobile.ottieniProprietario(idImmobile) !== idUtente){
    return next(new HttpError('Impossibile eliminare immobile non posseduto'), 404);
  }

  try{
      Immobile.eliminaImmobile(idImmobile);
  }catch(err){
      return next(err);
  }    
      
  res.json("Eliminato con successo.");   
}
 
const mostraImmobile = async (req, res, next) => {
 
  idImmobile = req.params.immobileId;
  let immobile;

  try{
    immobile = Immobile.getImmobile(idImmobile);
  }catch(err){
    return next(err);
  }
 
  res.json({immobile});
}
 
//ciao
exports.creaImmobile = creaImmobile;
exports.sponsorizzaImmobileById = sponsorizzaImmobileById;
exports.mostraImmobile = mostraImmobile;
exports.eliminaImmobile = eliminaImmobile;
exports.patchImmobileById = patchImmobileById;
exports.getImmobiliByIdCliente = getImmobiliByIdCliente;