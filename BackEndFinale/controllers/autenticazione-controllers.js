const {validationResult} = require('express-validator');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const HttpError = require('../models/http-error');
const Utente = require('../models/utente');

const login = async(req, res, next) => {
    const {email, password} = req.body;

    let existingUser 

    try {
        existingUser = await Utente.emailEsiste(email)
    } catch(err) {
        return next(
            new HttpError('Autenticazione fallita, riprova più tardi', 500)
        );
    };
    
    if(!existingUser) {
        return next(
            new HttpError('Dati non validi, controllare i dati o recuperare la password.', 403)
        );
    }

    let passwordValida = false;
    
    try {
        passwordValida = await bcrypt.compare(password, existingUser.password);
    } catch(err) {
        return next(
            new HttpError('Non è stato possibile accedere, controlla le credenziali e prova nuovamente')
        );
    }

    if(!passwordValida) {
        return next(
            new HttpError('Dati non validi, controllare i dati o recuperare la password', 403)
        )
    }

    let token;

    try {
        token = jwt.sign({userId: existingUser.id_utente}, 'stringa_segretissima_pippo', {expiresIn: '6h'})
    } catch(err) {
        return next(
            new HttpError('Autenticazione fallita, riprova più tardi', 500)
        );
    }

    res.status(201).json({userId: existingUser.id_utente,token: token});
    
}

const invioMailRecupero = async (req, res, next) => {
    const email = req.body;

    //Implementare invio email con route per recuperare password;
}

const recuperaDati = async(req, res, next) => {
    const{email, domandaSicurezza, ripostaSicurezza, nuovaPassword} = req.body;

    let existingUser;
    try {
        existingUser = Utente.emailEsiste(email)
    } catch(err) {
        return next(
            new HttpError('Errore nel reset della password, riprovare più tardi', 500)
        );
    }
    
    if(!existingUser) {
        return next(
            new HttpError("Email non valida, inserire un'email esistente e riprovare.", 422)
        )
    }

    if(existingUser.domandasicurezza != domandaSicurezza || existingUser.ripostasicurezza != ripostaSicurezza) {
        return next(
            new HttpError("Domanda o risposta di sicurezza non validi, riprovare", 422)
        );1
    }

    let hashedPassword;

    try {
        hashedPassword = bcrypt(nuovaPassword, 11);
    } catch(err) {
        return next(
            new HttpError('Errore nel reset della password, riprovare più tardi', 500)
        )
    }

    try {
        Utente.resetPassword(existingUser.id_utente, hashedPassword)
    } catch(err) {
        return next(
            new HttpError('Errore nel reset della password, riprovare più tardi', 500)
        );
    }

    res.sendStatus(200);
       
}

exports.login = login;
exports.invioMailRecupero = invioMailRecupero;
exports.recuperaDati = recuperaDati;