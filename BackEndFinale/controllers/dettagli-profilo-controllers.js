const Utente = require('../models/utente');
const Prenotazione = require('../models/prenotazione');
const HttpError = require('../models/http-error');
const {validationResult} = require('express-validator');
 //d
const getInformazioni = async (req, res, next) =>{
 
    const idUtente = req.userData.userId;
    let utente;
 
    try{
        utente = await Utente.ottieniInformazioniProfilo(idUtente);
    }catch(err){
        return next(err);
    }
    res.status(200).json(res.status(200).json({utente}));
}
 
const modificaProfilo = async (req, res, next) => {
    
    const errrors = validationResult(req);
 
    if(!errrors.isEmpty()){
        return next(new HttpError('Nessun parametro presente', 404));
    }
 
    const idUtente = req.userData.userId;
    const {nome, cognome, data_nascita, email, password, telefono} = req.body;
 
    try{
         Utente.modificaInformazioniProfilo(idUtente, nome, cognome, data_nascita, email, password, telefono);
    }catch(err){
        return next(err);
    }
 
    res.status(200).json('Profilo modificato con successo');
}
 
const getCronologiaGuadagni = async (req, res, next) =>{
 
    const idUtente = req.userData.userId;
    const dataOdierna = new Date();
    let immobili;
 
    try{
        immobili = await  Prenotazione.ottieniRiepilogoGuadagni(idUtente, dataOdierna)
    }catch(err){
        return next(err);
    }
  
    if(immobili.length){
        let prezzoTotale = 0;
        for(let i = 0; i < immobili.length; i++) {
            prezzoTotale += immobili[i].costo_totale;
        }
        res.status(200).json({prenotazioni_terminate: immobili, guadagno_totale: prezzoTotale});
    }else{
        res.status(200).json('Non sono presenti prenotazioni accettate');
    }                                    
}
const getPrenotazioniRicevute = async (req, res, next) =>{//tutte le prenotazioni che devo ancora accettare
 
    const idUtente = req.userData.userId;
    let prenotazioniDaAccettare;
    let prenotazioniAccettate;
 
    try{
        prenotazioniDaAccettare = await Prenotazione.listaPrenotazioniRicevuteDaAccettare(idUtente);
        prenotazioniAccettate = await Prenotazione.listaPrenotazioniRicevuteAccettate(idUtente);
    }catch(err){
        return next(err);
    }
    
    if(prenotazioniDaAccettare.length || prenotazioniAccettate.length){
        res.status(200).json({lista_prenotazioni_da_accettare: prenotazioniDaAccettare, lista_prenotazioni_accettate: prenotazioniAccettate});
    }else{
        res.status(200).json('Nessuna prenotazione ricevuta');
    }
}
 
const getPrenotazioniEffettuate = async (req, res, next) =>{
 
    const idUtente = req.userData.userId;
    let prenotazioni;
 
    try{
        prenotazioni = await Prenotazione.listaPrenotazioniEffettuate(idUtente);
    }catch(err){
        return next(err);
    }
    
    if(prenotazioni.length){
        res.status(200).json({prenotazioni_effettuate: prenotazioni});
    }else{
        res.status(200).json('Nessuna prenotazione effettuata');
    }
}
 
const accettaPrenotazione = async (req, res, next) =>{//da fare il controllo di proprietario
 
    const idPrenotazione = req.params.prenotazioneId;
 
   try{
       Prenotazione.accettaPrenotazione(idPrenotazione);
   }catch(err){
       return next(err);
   }
 
    res.status(200).json('Prenotazione accettata')
}
 
const rifiutaPrenotazione = async (req, res, next) =>{
 
    const idPrenotazione = req.params.prenotazioneId;
 
   try{
       Prenotazione.rifiutaPrenotazione(idPrenotazione);
   }catch(err){
       return next(err);
   }
 
    res.json('Prenotazione rifiutata')
}
 
exports.getInformazioni = getInformazioni;
exports.modificaProfilo = modificaProfilo;
exports.getCronologiaGuadagni = getCronologiaGuadagni;
exports.getPrenotazioniRicevute = getPrenotazioniRicevute;
exports.getPrenotazioniEffettuate = getPrenotazioniEffettuate;
exports.accettaPrenotazione = accettaPrenotazione;
exports.rifiutaPrenotazione = rifiutaPrenotazione;