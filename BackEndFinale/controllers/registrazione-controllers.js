const {validationResult} = require('express-validator');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const moment = require('moment');

const HttpError = require('../models/http-error');
const Utente = require('../models/utente');

const registrazione = async (req, res, next) => {
    const errors = validationResult(req);

    if(!errors.isEmpty()) {
        return next(
            new HttpError('Compilare tutti i campi obbligatori.', 422)
        );
    }

    const {nome, cognome, data_nascita, email, password, telefono, domandaSicurezza, rispostaSicurezza} = req.body;

    let existingUser;

    try {
        existingUser = await Utente.emailEsistente(email)
    } catch(err) {
        return next(
            new HttpError("Registrazione fallita, riprova più tardi", 500) 
        );
    }

    if(existingUser) {
        return next(
            new HttpError('Utente già registrato, effettua il login o recupera la password.', 422)
        );
    }

    let hashedPassword; 

    try {
        hashedPassword = await bcrypt.hash(password, 11);
    }  catch(err) {
        return next( 
            new HttpError("Registrazione fallita, riprova più tardi", 500)
        );
    }
    
    let utenteCreato;

    try{
       utenteCreato = await Utente.inserisciUtente(nome, cognome, moment(new Date(data_nascita)), email, hashedPassword, telefono, domandaSicurezza, rispostaSicurezza)
    } catch(err) {
        return next(
            new HttpError("Registrazione fallita, riprova più tardi", 500)
        );
    }

    let token;

    try {
        token = jwt.sign({userId: utenteCreato.id_utente}, 'stringa_segretissima_pippo', {expiresIn: '6h'})
    } catch(err) {
        return next(
            new HttpError('Registrazione fallita, riprova più tardi', 500)
        );
    }

    res.status(201).json({utenteId: utenteCreato.id_utente, token: token})
}

exports.registrazione = registrazione;