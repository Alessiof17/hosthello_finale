const { validationResult } = require('express-validator');
const nodemailer = require('nodemailer');

const HttpError = require('../models/http-error');
const db = require('../config/database');
const Informazioni = require('../models/Informazioni');
const Prenotazione = require('../models/prenotazione');
const Immobile = require('../models/immobile');
const Log_datiturismo = require('../models/log_datiturismo');

//Ottiene le informazioni per il settore turismo inerenti a una prenotazione
const getInfoTurismoByIdImmobile = async (req, res, next) => {
    const immobileId = req.params.iid;
    let fine_trimestre = new Date() //calcolo fine trimestre come data odierna, ovvero data in cui l'alert è suonato
    let inizio_trimestre = new Date();
    inizio_trimetre = inizio_trimestre.setMonth(fine_trimestre.getMonth() - 3); //calcolo inizio trimestre sottraendo tre mesi alla fine_trimestre

    let numero_notti_pagamento;
    let numero_notti_esenzione;
    let riepilogo_prenotazioni_trimestre;
    try {
        riepilogo_prenotazioni_trimestre = await Prenotazione.getInformazioniTrimestre(inizio_trimestre,fine_trimestre, immobileId);
        numero_notti_pagamento = await Prenotazione.ottieniNumeroNotti(inizio_trimestre, fine_trimestre, immobileId, 0) //Con 0 intendiamo che non hanno esenzione
        numero_notti_esenzione = await Prenotazione.ottieniNumeroNotti(inizio_trimestre, fine_trimestre, immobileId, 1) // Con 1 hanno esenzione
    } catch(err) {
        return next(
            new HttpError('Non sono state trovate informazioni per questo immobile, inserirle prima di inviare resoconto.', 404)
        );
    }
    
    res.status(200).json({riepilogo: riepilogo_prenotazioni_trimestre, numero_notti_esenzione: numero_notti_esenzione, numero_notti_pagamento: numero_notti_pagamento});   
}

const inserisciInformazioni = async(req, res, next) => {
    const errors = validationResult(req);

    if(!errors.isEmpty()) {
        return next(new HttpError('Compilare tutti i campi obbligatori', 422))
    }

    const ref_prenotazione = req.userData.userId;
    let {nome_persona, cognome_persona, data_nascita_persona, città_residenza, provincia_residenza,
        indirizzo_residenza, esenzione} = req.body;
    data_nascita_persona = new Date(data_nascita_persona);

    let utente;
    try {
        
    utente = await Informazioni.aggiungiInformazioni(nome_persona, cognome_persona, data_nascita_persona, città_residenza, provincia_residenza,
        indirizzo_residenza, esenzione)
    } catch(err) {
        return next(
            new HttpError('Errore nel caricare le informazioni, provare più tardi', 500)
        );
    }

    //inserire invio alla questura dati e upload documento
    res.status(201).send("Cliente aggiunto correttamente.")   
}

const inviaInformazioniTurismo = async(req, res, next) => {
    const errors = validationResult(req);
    

    if(!errors.isEmpty()) {
        return next(new HttpError('Compilare tutti i campi obbligatori', 422))
    }
    
    const{numero_notti_pagamento, numero_notti_esenzione} = req.body;
    const immobileId = req.params.iid;
    const data_odierna = new Date();

    try {
        Log_datiturismo.create({data_invio: data_odierna, ref_immobile: immobileId, 
            numero_notti_esenzione: numero_notti_esenzione, numero_notti_paganti: numero_notti_pagamento})
    } catch(err) {
        return next(
            new HttpError("Errore nell'invio dei dati, provare più tardi", 500)
        );
    }
    


    //nodemailer OAuth2
    /*var transporter = nodemailer.createTransport({
        service: 'gmail',
        secure: false, // use SSL
        port: 25, // port for secure SMTP
        auth: {
          user: '17nebula@gmail.com',
          pass: '
        },
        tls: {
            rejectUnauthorized: false
        }
      });
      
      var mailOptions = {
        from: '17nebula@gmail.com',
        to: 'alessio.ferrara17@gmail.com',
        subject: 'Resoconto settore turismo',
        text: 'prova'
      };
      
      transporter.sendMail(mailOptions, function(error, info){
        if (error) {
          console.log(error);
        } else {
          console.log('Email sent: ' + info.response);
        }
      }); */

      res.sendStatus(200);
}

const getImmobiliUtente = async (req, res, next) => {
    const utenteId = req.params.uid;
    let immobili;

    try {
        immobili = await Immobile.getImmobiliByIdUtente(utenteId);   //Ottengo tutti gli immobili dell'utente per permettere l'invio delle informazioni.
    } catch(err) {
        return next(
            new HttpError('Errore generico. Provare più tardi', 500)
        );
    }

    if(immobili.length) 
      res.status(200).json(immobili);
    else
      res.status(404).send('Nessun immobile trovato');
}

const getImmobiliTurismo = async (req, res, next) => {  //Ottiene immobili per cui non è ancora stato inviato il resoconto trimestrale.
    const utenteId = req.userData.userId;
    let immobili_turismo;

    try {
        immobili_turismo = await Immobile.getImmobiliTurismoByIdUtente(utenteId)
    } catch(err) {
        return next(
            new HttpError("Impossibile recuperare le informazioni, riprova più tardi", 500)
        );
    }
    if(immobili_turismo.length)
        res.status(200).json(immobili_turismo);
    else
        res.status(404).send("Non sono stati trovati immobili per cui bisogna mandare il resoconto.");
    
}

//funzioni per invio notifica? Gestire nell'header del frontend. O notifiche push? => pusher.com ajax
exports.getImmobiliTurismo = getImmobiliTurismo
exports.getImmobiliUtente = getImmobiliUtente;
exports.getInfoTurismoByIdImmobile  = getInfoTurismoByIdImmobile;
exports.inviaInformazioniTurismo = inviaInformazioniTurismo;
exports.inserisciInformazioni = inserisciInformazioni;