const HttpError = require('../models/http-error');
const { validationResult } = require('express-validator');

const Prenotazione = require('../models/prenotazione');
const Immobile = require('../models/immobile');
const Composta = require('../models/composta');


//Effettua la ricerca degli immobili disponibili nell'intervallo di date selezionato.
const getImmobiliNonPrenotati = async (req, res, next) => {

  const errors = validationResult(req);
  if(!errors.isEmpty()){
    res.status(422).send("Dati non validi, riprova.");
  }

  let {città, provincia, regione, tipo_alloggio, data_arrivo, data_partenza, ospiti, costo} = req.body;
  data_arrivo = new Date(data_arrivo);
  data_partenza = new Date(data_partenza);

  let immobili;

  try {
      immobili = await Immobile.getImmobiliNonPrenotati(città, provincia, regione, tipo_alloggio, data_arrivo, ospiti, costo, data_partenza)
  } catch(err) {
      return next(err)
  }
  
  //Mostriamo per primi gli immobili sponsorizzati. Tra due immobili sponsorizzati, diamo la priorità a quelli con costo inferiore
  if(immobili.length) {
    const immobili_ordinati = immobili.sort(function(a, b) {
      if(a.isSponsorizzata > b.isSponsorizzata || a.costo_notte < b.costo_notte) 
          return -1
      if(a.isSponsorizzata < b.isSponsorizzata || a.costo_notte > b.costo_notte)
          return 1
      return 0
    });
    res.status(200).json(immobili_ordinati);
  }
  else {
    res.status(404).send("Non è stato trovato nessun immobile.");
  }
}


//Applica filtri su numero camere, numero bagni e posti letto
const applicaFiltri = async (req, res, next) => { 
  
  const errors = validationResult(req);
  if(!errors.isEmpty()){
    res.status(422).send("Dati non validi, riprova.");
  }

  const {città, provincia, regione, tipo_alloggio, data_arrivo, data_partenza, ospiti, costo, numero_camere, numero_bagni, posti_letto} = req.body;

  let immobili_liberi;

  try {
      immobili_liberi = await Immobile.getImmobiliNonPrenotati(città, provincia, regione, tipo_alloggio, 
          data_arrivo, ospiti, costo, data_partenza)
  } catch(err) {
      return next(
          new HttpError("Errore. Riprova in seguito", 500)
      )
  }

  const immobili_filtrati_ordinati = immobili_liberi.filter(immobile_l => { 
      if(immobile_l.numero_stanze >= numero_camere && immobile_l.numero_bagni >= numero_bagni && immobile_l.posti_letto >= posti_letto)
          return true;
      else
          return false;
  }).sort(function(a, b) {
      if(a.isSponsorizzata > b.isSponsorizzata || a.costo_notte < b.costo_notte) 
          return -1
      if(a.isSponsorizzata < b.isSponsorizzata || a.costo_notte > b.costo_notte)
          return 1
      return 0
  }) 
  if(posti_letto < ospiti){
    return next(
      new HttpError('Effettuare la ricerca con nuovi parametri. Inserire nuovo numero ospiti.', 500)
    );
  }
  if(immobili_filtrati_ordinati.length > 0){
    res.status(200).json(immobili_filtrati_ordinati);}
  else {
    res.status(404).send("Non sono disponibili strutture con questi parametri.");
  }
}


//Restituisce tutte le info di un immobile selezionato per iid, compresi i servizi.
const getImmobileById = async (req, res, next) => {

  const idImmobile = req.params.iid;

  let descrizione_immobile;

  try{
    descrizione_immobile = await Composta.OttieniImmobileById(idImmobile)
  } catch (err){
    return next(err);
  }
  res.status(200).json(descrizione_immobile);
}


//Restituisce le informazioni dell'host (nome, cognome, email, cellulare)
const getInformazioniHost = async (req, res, next) => {

    const idImmobile = req.params.iid;

    let informazioniHost;

    try{
      informazioniHost = await Immobile.OttieniInfoHost(idImmobile)
    } catch (err){
      return next(err);
    }
    res.status(200).json(informazioniHost);
  }


  //verifica che l'immobile sia disponibile per le date selezionate. Dopodiché verifica che l'utente non abbia superato la soglia dei 28 giorni annui
  const verificaDisponibilita = async (req, res, next) => {

    const errors = validationResult(req);
    if(!errors.isEmpty()){
      res.status(422).send("Dati non validi, riprova.");
    }

    const idImmobile = req.params.iid;
    const idCliente = req.userData.userId;

    const {data_inizio, data_fine, numero_ospiti} = req.body;
    const date1 = new Date(data_inizio);
    const date2 = new Date(data_fine);
    const diffTime = Math.abs(date2 - date1);
    const numeroGiorni = Math.ceil(diffTime / (1000 * 60 * 60 * 24));

    let verifica;

    try{
      verifica = await Prenotazione.OttieniDisponibilita(idImmobile, date1, date2)
      } catch (err){
        return next(err);
        }
    
    if(verifica){
      res.status(201).json("Non disponibile nell'intervallo selezionato. Prova a cambiare date."); 
    } 
    else {
          let verificaGiorni;
          try{
            verificaGiorni = await Prenotazione.Verifica28Giorni(idImmobile, idCliente)
          }
          catch(err){
            return next(err);
          }
          let sommaGiorni = numeroGiorni;
          for (let i = 0; i < verificaGiorni.length; i++){
            sommaGiorni += verificaGiorni[i].numero_giorni;
          }
      
          let differenzaGiorni = sommaGiorni - numeroGiorni;
          let giorniRimanenti = 28 - differenzaGiorni;
          if(sommaGiorni > 28 && differenzaGiorni < 28){
            res.json("Stai superando la soglia dei 28 giorni annui per questo immobile. Hai già soggiornato qui per " + differenzaGiorni + " giorni, pertanto puoi cambiare le date e prenotare per un massimo di " + giorniRimanenti + " giorni.")
          } else {
            if(differenzaGiorni >= 28){
              res.json("Hai superato la soglia dei 28 giorni annui per questo immobile. Prova a cercare un'altra struttura!")
            } else {
              let costoNotte;
                try{
                  costoNotte = await Immobile.OttieniCostoNotte(idImmobile)
                  } catch(err){
                    return next(err);
                    }
                    let costoTotale = costoNotte.costo_notte * numeroGiorni + 10 * numero_ospiti;
                    res.status(201).json({data_inizio: date1.toLocaleDateString(), data_fine: date2.toLocaleDateString(), numero_ospiti: numero_ospiti, numero_giorni: numeroGiorni, costo_totale: Math.round(costoTotale)}); 
                  }
              }
            }
  }


  //aggiunge la prenotazione nel database
  const creaPrenotazione = async (req, res, next) =>{

  const errors = validationResult(req);
  if(!errors.isEmpty()){
    res.status(422).send("Dati non validi, riprova.");
    }

    const idImmobile = req.params.iid;
    const idCliente = req.userData.userId;
    const {data_inizio, data_fine, numero_ospiti, numero_giorni, costo_totale} = req.body; 
    let nuovaPrenotazione;

    try{
      nuovaPrenotazione = await Prenotazione.CreaPrenotazione(idImmobile, idCliente, data_inizio, data_fine, costo_totale, numero_ospiti, numero_giorni)
    } catch (err){
      return next(err);
    }
    res.status(200).json(nuovaPrenotazione);
  }


exports.getImmobiliNonPrenotati = getImmobiliNonPrenotati;
exports.applicaFiltri = applicaFiltri;
exports.getImmobileById = getImmobileById;
exports.getInformazioniHost = getInformazioniHost;
exports.verificaDisponibilita = verificaDisponibilita;
exports.creaPrenotazione = creaPrenotazione;
