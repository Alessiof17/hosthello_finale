const express = require('express');
const { check } = require('express-validator');
const checkAuth = require('../middleware/check-auth');

const RicercaControllers = require('../controllers/ricerca-controllers');


const router = express.Router();

router.post('/', [
    check('città').not().isEmpty(),
    check('tipo_alloggio').not().isEmpty(),
    check('data_arrivo').not().isEmpty(),
    check('data_partenza').not().isEmpty(),
    check('ospiti').not().isEmpty(),
    check('costo').not().isEmpty()], RicercaControllers.getImmobiliNonPrenotati);
router.post('/ricerca', [
    check('città').not().isEmpty(),
    check('tipo_alloggio').not().isEmpty(),
    check('data_arrivo').not().isEmpty(),
    check('data_partenza').not().isEmpty(),
    check('ospiti').not().isEmpty(),
    check('costo').not().isEmpty()], RicercaControllers.applicaFiltri);
router.get('/ricerca/:immobileId', RicercaControllers.getImmobileById);
router.get('/ricerca/immobile/:immobileId', RicercaControllers.getInformazioniHost);

router.use(checkAuth);

router.post('/ricerca/immobile/:immobileId', [
    check('data_inizio').not().isEmpty(),
    check('data_fine').not().isEmpty(),
    check('numero_ospiti').not().isEmpty()], RicercaControllers.verificaDisponibilita);
router.post('/ricerca/immobile/riepilogo/:immobileId', [
    check('data_inizio').not().isEmpty(),
    check('data_fine').not().isEmpty(),
    check('numero_ospiti').not().isEmpty(),
    check('numero_giorni').not().isEmpty(),
    check('costo_totale').not().isEmpty()], RicercaControllers.creaPrenotazione);

//iid = immobileId
//id = userId

module.exports = router;