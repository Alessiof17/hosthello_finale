const express = require('express');
const { check } = require('express-validator');

const RegistrazioneController = require('../controllers//registrazione-controllers');

const router = express.Router();

router.post(
    '/', 
    check(['nome', 'cognome', 'data_nascita', 'email', 'password', 'telefono', 'domandaSicurezza', 'rispostaSicurezza'])
    .not()
    .isEmpty(), 
    RegistrazioneController.registrazione
);


module.exports = router;