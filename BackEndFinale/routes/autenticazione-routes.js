const express = require('express');
const { check } = require('express-validator');

const AutenticazioneController = require('../controllers/autenticazione-controllers');

const router = express.Router();

router.post('/login', check(['email', 'password']).not().isEmpty(), AutenticazioneController.login);
router.post('/inviaemailrecupero', check(['email']).not().isEmpty(), AutenticazioneController.invioMailRecupero);
router.post('/reimpostapassword', check(['email', 'domandaSicurezza', 'rispostaSicurezza', 'nuovaPassword']).not().isEmpty(), AutenticazioneController.recuperaDati);

module.exports = router;