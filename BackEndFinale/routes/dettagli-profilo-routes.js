const express = require('express');
const {check} = require('express-validator');
const checkAuth = require('../middleware/check-auth');
 
const dettagliProfiloControllers = require('../controllers/dettagli-profilo-controllers');

 
const router = express.Router();

router.use(checkAuth);
router.get('/modificaprofilo/', dettagliProfiloControllers.getInformazioni);//li ritorno alla view prima della modifica
router.get('/cronologiaguadagni/', dettagliProfiloControllers.getCronologiaGuadagni);
router.get('/prenotazioniricevute/', dettagliProfiloControllers.getPrenotazioniRicevute);//dare tutte le prenotazioni sia da accettare sia già accettate
router.get('/prenotazionieffettuate/', dettagliProfiloControllers.getPrenotazioniEffettuate);
router.get('/', dettagliProfiloControllers.getInformazioni);    //riepilogo dettagli profilo
router.patch('/modificaprofilo/submit/', check(['nome', 'cognome', 'data_nascita', 'email', 'password', 'telefono']).not().isEmpty(), dettagliProfiloControllers.modificaProfilo);//effettua la modifica vera e propria dopo il submit
router.patch('/prenotazioniricevute/:prenotazioneId', dettagliProfiloControllers.accettaPrenotazione);
router.delete('/prenotazioniricevute/:prenotazioneId', dettagliProfiloControllers.rifiutaPrenotazione);
//d
module.exports = router;