const express = require('express');
const { check } = require('express-validator');
const checkAuth = require('../middleware/check-auth');

const BurocraziaController = require('../controllers/burocrazia-controllers');

const router = express.Router();

router.use(checkAuth);
router.get('/settoreturismo/', BurocraziaController.getImmobiliTurismo) //visualizza a schermo tutti gli immobili per cui l'utente non ha inviato il resoconto.
router.get('/settoreturismo/immobile/:immobileId', BurocraziaController.getInfoTurismoByIdImmobile) //permette all'utente di modificare i campi settore turismo relativi ad un immobile
router.post(                                    //Aggiungere blocco se si prova ad inviare qualcosa che è stato già inviato
        '/settoreturismo/submit/:immobileId',
        check(['numero_notti_pagamento', 'numero_notti_esenzione']).not().isEmpty(),
        BurocraziaController.inviaInformazioniTurismo) //invia i dati modificati dall'utente al settore turismo.
router.get('inserisciinformazioni/', BurocraziaController.getImmobiliUtente) // get che visualizza a schermo tutti gli immobili di un utente per cui inviare le informazioni. 
router.post(
        '/inserisciinformazioni/:prenotazioneId', 
        check(['nome_persona', 'cognome_persona', 'data_nascita_persona', 'città_residenza', 'provincia_residenza','indirizzo_residenza'])
        .not()
        .isEmpty(),
        BurocraziaController.inserisciInformazioni
    )//permette all'utente d'inserire le informazioni e inviarle alla questura. 
    
module.exports = router;