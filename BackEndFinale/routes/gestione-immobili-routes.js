const express = require('express');
const { check } = require('express-validator');
const checkAuth = require('../middleware/check-auth');
 
const ImmobiliControllers = require('../controllers/gestione-immobili-controllers');

 
const router = express.Router();

router.use(checkAuth);
 
router.get('/', ImmobiliControllers.getImmobiliByIdCliente);   //Ottenere gli immobili per cliente
router.get('/mostraimmobile/:immobileId', ImmobiliControllers.mostraImmobile);//mostra l'immobile
router.get('/modificaimmobile/:immobileId', ImmobiliControllers.mostraImmobile);//da i dati al front end per modificare l'immobile
router.post('/aggiungiimmobile/',  check(['nome_immobile', 'citta', 'provincia', 'regione', 'indirizzo', 'costo', 'numero_stanze', 'posti_letto', 'numero_bagni']).not().isEmpty(), ImmobiliControllers.creaImmobile);
router.patch('/modificaimmobile/submit/:immobileId', check(['citta', 'provincia', 'regione', 'indirizzo', 'costo', 'numero_stanze', 'posti_letto', 'numero_bagni']).not().isEmpty(), ImmobiliControllers.patchImmobileById);  
router.patch('/sponsorizzaimmobile/:immobileId', ImmobiliControllers.sponsorizzaImmobileById);  //sponsorizza immobile
router.delete('/:immobileId', ImmobiliControllers.eliminaImmobile); //eliminare immobile
 //d
module.exports = router;
