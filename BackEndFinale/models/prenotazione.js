const { v4: uuid } = require("uuid");
const Sequelize = require('sequelize');
const moment = require('moment');
const Utente = require('./utente');
const Immobile = require('./immobile');
const db = require('../config/database');
const Informazioni = require('./Informazioni');


const Prenotazione = db.define('prenotazione', {
    id_prenotazione: {
      type: Sequelize.STRING(100),
      allowNull: false,
      primaryKey: true
    },
    ref_utente_prenotazione: {
      type: Sequelize.STRING(100),
      allowNull: false,
      references: {
        model: Utente,
        key: 'id_utente'
      }
    },
    ref_immobile: {
      type: Sequelize.STRING(100),
      allowNull: false,
      references: {
        model: Immobile,
        key: 'id_immobile'
      }
    },
    data_inizio: {
      type: Sequelize.DATEONLY,
      allowNull: false
    },
    data_fine: {
      type: Sequelize.DATEONLY,
      allowNull: false
    },
    numero_notti: {
      type: Sequelize.INTEGER,
      allowNull: false
    },
    costo_totale: {
      type: Sequelize.FLOAT,
      allowNull: false
    },
    numero_ospiti: {
      type: Sequelize.INTEGER,
      allowNull: false
    },
    accettata: {
      type: Sequelize.INTEGER(1),
      allowNull: false
    }
  }, {
    table_name: 'prenotazione',
    freezeTableName: true
  })

Prenotazione.OttieniInformazioniPrenotazione = async function(immobileId) {
  return await Prenotazione.findAll({
      attributes: [],
      where: {
          ref_immobile : immobileId,
          accettata : 1,
          data_fine : {
            [Op.gt] : new Date()
          }
      },
      include : [{
          model: Informazioni,
          require: true
      }]
  });
}

Prenotazione.ottieniNumeroNotti = async function(inizio_trimestre, fine_trimestre, id_immobile, valore) {
  let numero_notti_ospiti;

  try {
    
    numero_notti_ospiti = await Prenotazione.findAll({
      raw: true,
      attributes: [[Sequelize.fn('sum', Sequelize.col('numero_notti')), 'sum_g']],
      where: {
        data_inizio: {
          [Op.gt] :  inizio_trimestre
        },
        data_fine: {
          [Op.lt] : fine_trimestre
        },
        ref_immobile: id_immobile
      },
      include: [{
        model: Informazioni,
        required: true,
        where: {
          esenzione : valore
        },
        attributes : []
      }]
    })  

    if(numero_notti_ospiti[0].sum_g == null)
      throw Error("Non sono presenti informazioni in merito a questo immobile")
  } catch (err) {
      throw err
  }
  
  
  return numero_notti_ospiti[0].sum_g;
  
  
}

Prenotazione.getInformazioniTrimestre = async function(inizio_trimestre, fine_trimestre, immobileId) {
  let informazioni_prenotazioni;
  
  try {
    informazioni_prenotazioni = await Prenotazione.findAll({
      raw: true,
      where:  {
        data_inizio: {
          [Op.gt] :  inizio_trimestre
        },
        data_fine: {
          [Op.lt] : fine_trimestre
        },
        ref_immobile: immobileId
      }
    })
  } catch(err) {
    throw err;
  }

  return informazioni_prenotazioni;
}

Prenotazione.OttieniInformazioniPrenotazione = async function(immobileId) {
  return await Prenotazione.findAll({
      attributes: [],
      where: {
          ref_immobile : immobileId,
          accettata : 1,
          data_fine : {
              [Op.gt] : moment.now()
          }
      },
      include : [{
          model: Informazioni,
      }]
  });
}

Prenotazione.OttieniDisponibilita = async function(idImmobile, data_arrivo, data_partenza) {
  return await Prenotazione.findOne({
    where: {
      [Op.and]: [{ref_immobile: idImmobile},{
        [Op.or]: [{
          data_inizio: {
            [Op.lte]: data_arrivo 
          },
          data_fine: {
            [Op.gte]: data_arrivo
          }
        },
        {
          data_inizio: {
            [Op.lte]: data_partenza 
          },
          data_fine: {
            [Op.gte]: data_partenza
          }
        }

        //data_inizio <= data_arrivo <= data_fine
        //data_inizio <= data_partenza <= data_fine
      ]}]
    }
  })
}

//restituisce le prenotazioni effettuate da un utente per un determinato immobile così da calcolare se ha superato i 28 giorni
Prenotazione.Verifica28Giorni = async function(idImmobile, idCliente) {
  return await Prenotazione.findAll({
      attributes: ['numero_giorni'],
      where: {
          [Op.and]: [{ref_immobile: idImmobile}, {ref_utente_prenotazione: idCliente}]
      }
  });
}

//aggiunge una nuova prenotazione nel database
Prenotazione.CreaPrenotazione = async function(idImmobile, idCliente, data_inizio, data_fine, costo_totale, numero_ospiti, numero_giorni) {
  return await Prenotazione.create({
    id_prenotazione: uuid(), ref_utente_prenotazione: idCliente,  ref_immobile: idImmobile, data_inizio: data_inizio, data_fine: data_fine, 
    numero_giorni: numero_giorni, costo_totale: costo_totale, numero_ospiti: numero_ospiti, accettata: 0
  });
}

Prenotazione.ottieniRiepilogoGuadagni = async function(idUtente, dataOdierna){
  return await  Prenotazione.findAll({attributes: ['id_prenotazione', 'data_inizio', 'data_fine', 'numero_ospiti', 'costo_totale'], 
  where: {accettata: 1, data_fine: {[Op.lt]: dataOdierna}}, 
  include: [{model: Utente, attributes: ['nome', 'cognome']}, 
  {model: Immobile, attributes: ['nome_immobile'], where: {ref_proprietario: idUtente}}]});
}

Prenotazione.listaPrenotazioniRicevuteDaAccettare = async function(idUtente){
  return await  Prenotazione.findAll(
    {attributes: ['id_prenotazione', 'data_inizio', 'data_fine', 'numero_notti', 'costo_totale', 'numero_ospiti'],
     where: {accettata: 0}, 
     include: [{model: Immobile, attributes:['nome_immobile'], where: {ref_proprietario: idUtente}}, {model: Utente, attributes:['nome', 'cognome']}]})
}

Prenotazione.listaPrenotazioniRicevuteAccettate = async function(idUtente){//controllare where nell'include
  return await Prenotazione.findAll(
    {attributes: ['id_prenotazione', 'data_inizio', 'data_fine', 'numero_notti', 'costo_totale', 'numero_ospiti'],
     where: {accettata: 1}, 
     include: [{model: Immobile, attributes:['nome_immobile'], where: {ref_proprietario: idUtente}}, {model: Utente, attributes:['nome', 'cognome']}]})
}

Prenotazione.listaPrenotazioniEffettuate = async function(idUtente){
  return await  Prenotazione.findAll({attributes: ['id_prenotazione', 'data_inizio', 'data_fine', 'numero_notti', 'costo_totale', 'numero_ospiti'], 
  where: {ref_utente_prenotazione: idUtente}, 
  include: [{model: Immobile, attributes: ['nome_immobile'], where: {id_immobile: ref_immobile}}]})
}

Prenotazione.accettaPrenotazione = async function(idPrenotazione){
  return await  Prenotazione.update({accettata: 1}, {where: {id_prenotazione: idPrenotazione}});
}

Prenotazione.rifiutaPrenotazione = async function(idPrenotazione){
  return await  Prenotazione.destroy({where: {id_prenotazione: idPrenotazione}});
}

Prenotazione.hasMany(Informazioni, {foreignKey: 'ref_prenotazione'});
Informazioni.belongsTo(Prenotazione, {foreignKey: 'ref_prenotazione', sourceKey: 'ref_prenotazione'});

module.exports = Prenotazione;