const { v4: uuid } = require("uuid");
const Sequelize = require('sequelize');

const db = require('../config/database');
const Prenotazione = require('./prenotazione');
const Immobile = require('./immobile');

const Utente = db.define('utente', {
  id_utente: {
    type: Sequelize.STRING(100),
    allowNull: false,
    primaryKey: true
  },
  nome: {
    type: Sequelize.STRING(100),
    allowNull: false
  },
  cognome: {
    type: Sequelize.STRING(100),
    allowNull: false
  },
  data_nascita: {
    type: Sequelize.DATEONLY,
    allowNull: false
  },
  email: {
    type: Sequelize.STRING(100),
    allowNull: false
  },
  password: {
    type: Sequelize.STRING(100),
    allowNull: false
  },
  telefono: {
    type: Sequelize.STRING(100),
    allowNull: false
  },
  domandasicurezza: {
    type: Sequelize.STRING(100),
    allowNull: false
  },
  rispostadomandas: {
    type: Sequelize.STRING(100),
    allowNull: false
  }
}, {
  tableName: 'utente'
});

Utente.ottieniInformazioniProfilo = async function(id_utente){
  return await  Utente.findAll({ where: {id_utente: id_utente}, attributes: ['nome', 'cognome', 'data_nascita', 'email', 'telefono']});
}
 
Utente.modificaInformazioniProfilo = async function(idUtente, nome, cognome, data, email, password, telefono){
  return await  Utente.update({nome: nome, cognome: cognome, data_nascita: data, email: email, password: password, telefono: telefono}, {where: {id_utente: idUtente}});
}

Utente.emailEsistente = async function(email_inserita) {
  
  try {
    return Utente.findOne({
      where: {
        email : email_inserita
      }
    })
  } catch(err) {
    throw err;
  }
}

Utente.inserisciUtente = async function(nome, cognome, data_nascita, email, hashedPassword, telefono, domandaSicurezza, rispostaSicurezza) {
  let utenteCreato;
  try {
    utenteCreato = await Utente.create({id_utente: uuid(), nome : nome, cognome : cognome, data_nascita: data_nascita, email: email, password: hashedPassword, telefono: telefono,
    domandasicurezza: domandaSicurezza, rispostadomandas: rispostaSicurezza})
  } catch(err) {
    throw err;
  }

  return utenteCreato;

}

Utente.resetPassword = async function(idUtente, hashedPassword) {
  try  {
    Utente.update({password : hashedPassword}, {
      where: {
        id_utente : idUtente
      }
    })
  } catch(err) {
    throw err;
  }
}

Utente.hasMany(Prenotazione, {foreignKey: 'ref_utente_prenotazione'});
Prenotazione.belongsTo(Utente, {foreignKey: 'ref_utente_prenotazione', sourceKey: 'id_utente'});

module.exports = Utente;