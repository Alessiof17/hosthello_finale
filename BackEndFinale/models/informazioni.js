const Sequelize = require('sequelize');
const db = require('../config/database');
const Prenotazione = require('./prenotazione');

const Informazioni = db.define('informazioni', {
    id_info: {
      type: Sequelize.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    nome_persona: {
      type: Sequelize.STRING(100),
      allowNull: false
    },
    cognome_persona: {
      type: Sequelize.STRING(100),
      allowNull: false
    },
    data_nascita_persona: {
      type: Sequelize.DATEONLY,
      allowNull: false
    },    
    esenzione: {
      type: Sequelize.INTEGER(1),
      allowNull: false
    }
}, {
  freezeTableName: true,
  tableName: 'informazioni', 
});

Informazioni.aggiungiInformazioni = async function(nome_persona, cognome_persona, data_nascita_persona, 
  città_residenza, provincia_residenza, indirizzo_residenza, esenzione) {
  let utente;
  try {  
    utente = await Informazioni.create({nome_persona:nome_persona,cognome_persona:cognome_persona, data_nascita_persona: data_nascita_persona, 
            città_residenza: città_residenza, provincia_residenza: provincia_residenza, 
            indirizzo_residenza:indirizzo_residenza, ref_prenotazione: ref_prenotazione, esenzione: esenzione});
  }catch(err) {
    throw err;
  }
  
  return utente;
}

module.exports = Informazioni; 
