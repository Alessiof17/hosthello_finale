const Sequelize = require('sequelize');
const db = require('../config/database');
const Immobile = require('./immobile');

const Log_datiturismo = db.define('log_datiturismo', {
    data_invio : {
        type: Sequelize.DATEONLY,
        allowNull: false
    },
    ref_immobile: {
        type: Sequelize.STRING(100),
        allowNull: false,
        references: {
            model: Immobile,
            key: 'id_immobile'
        }
    },
    id_log: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    numero_notti_esenzione: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    numero_notti_paganti: {
        type: Sequelize.IndexHints,
        allowNull: false
    }
},
{
    table_name: 'log_datiturismo',
    freezeTableName: true
});

module.exports = Log_datiturismo;