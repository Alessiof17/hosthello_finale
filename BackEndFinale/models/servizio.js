const Sequelize = require('sequelize');
const db = require('../config/database');

const Servizio = db.define('servizio', {
    id_servizio: {
      type: Sequelize.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    descrizione: {
      type: Sequelize.STRING(100),
      allowNull: false
    }
  }, {
    freezeTableName: true,
    tableName: 'servizio'
});

module.exports = Servizio;

