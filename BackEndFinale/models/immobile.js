const Sequelize = require('sequelize');
const Op = Sequelize.Op;

const db = require('../config/database');
const Utente = require('./utente');
const Servizio = require('./servizio');
const Prenotazione = require('./prenotazione');
const Log_datiturismo = require('./log_datiturismo');


const Immobile = db.define('immobile', {
  id_immobile: {
    type: Sequelize.STRING(100),
    allowNull: false,
    primaryKey: true
  },
  città_immobile: {
    type: Sequelize.STRING(100),
    allowNull: false
  },
  provincia_immobile: {
    type: Sequelize.STRING(100),
    allowNull: false
  },
  regione_immobile: {
    type: Sequelize.STRING(100),
    allowNull: false
  },
  indirizzo_immobile: {
    type: Sequelize.STRING(100),
    allowNull: false
  },
  costo_notte: {
    type: Sequelize.FLOAT,
    allowNull: false
  },
  numero_stanze: {
    type: Sequelize.INTEGER,
    allowNull: false
  },
  posti_letto: {
    type: Sequelize.INTEGER,
    allowNull: false
  },
  numero_bagni: {
    type: Sequelize.INTEGER,
    allowNull: false
  },
  ref_proprietario: {
    type: Sequelize.STRING(100),
    allowNull: false,
    references: {
      model: 'utente',
      key: 'id_utente'
    }
  },
  data_inizio_spons: {
    type: Sequelize.DATEONLY,
    allowNull: true
  },
  data_fine_spons: {
    type: Sequelize.DATEONLY,
    allowNull: false
  },
  descrizione: {
    type: Sequelize.STRING(1000),
    allowNull: false
  },
  punti_interesse: {
    type: Sequelize.STRING(1000),
    allowNull: true
  },
  nome_immobile: {
    type: Sequelize.STRING(100),
    allowNull: false
  },
  tipo_struttura: {
    type: Sequelize.STRING(100),
    allowNull: false
  }
},
 {
  freezeTableName: true,
  tableName: 'immobile'
});

Immobile.getImmobiliByIdCliente = async function(utenteId) {
  let immobili;
  try {
    immobili = await Immobile.findAll({
      raw: true,
      attributes:['città_immobile', 'provincia_immobile', 'indirizzo_immobile', 'nome_immobile', 'tipo_struttura'],
      where: {
        ref_proprietario : utenteId,
      },
      include: [{
        model : Utente,
      }]
    })
  } catch(err) {
    return err
  }
  return immobili;

}

Immobile.getImmobiliTurismoByIdUtente = async function(utenteId){
  const oggi = new Date();
  let immobili_turismo;
  let immobili_log_inviato;
  const ref_immobili_inviato = [];

  try {
    immobili_log_inviato = await Log_datiturismo.findAll({  //Prendo gli immobili per cui oggi, data invio trimestre, ho inviato il resoconto
      raw: true,  
      attributes: ['ref_immobile'],                           
      where: {
        data_invio: oggi
      }
    })
  } catch(err) {
    return err;
  }

  console.log(immobili_log_inviato);
  immobili_log_inviato.forEach(immobile => {
    ref_immobili_inviato.push(immobile.ref_immobile);
  })

  try {
    immobili_turismo = await Immobile.findAll({
      where: {
        id_immobile: {
          [Op.notIn] : ref_immobili_inviato
        },
        ref_proprietario: utenteId
      }
    })
  } catch(err) {
    return err;
  }

  return immobili_turismo;
}
  

Immobile.getImmobiliNonPrenotati = async function(città, provincia, regione, tipo_alloggio, data_arrivo, ospiti, costo, data_partenza) {
  let immobili_prenotati;
  const ref_immobili_prenotati = [];
  let immobili_liberi;
  const data_odierna = new Date();
 
  try {
    immobili_prenotati = await Prenotazione.findAll({
      attributes: ['ref_immobile'],
      raw: true,
      where: {
        [Op.or]: [{
            data_inizio: {
              [Op.lte]: data_arrivo 
            },
            data_fine: {
              [Op.gte]: data_arrivo
            }
          },
          {
            data_inizio: {
              [Op.lte]: data_partenza 
            },
            data_fine: {
              [Op.gte]: data_partenza
            }
          }
 
          //data_inizio <= data_arrivo <= data_fine
          //data_inizio <= data_partenza <= data_fine
        ]
      }
  })
  } catch(err) {
    return err;
  }  
  
  immobili_prenotati.forEach(immobile => {    //Mi prendo il ref_immobile degli immobili prenotati
    ref_immobili_prenotati.push(immobile.ref_immobile);
  })
  
  try { //Mi calcolo gli immobili liberi come gl'immobili che non sono tra quelli prenotati.
    immobili_liberi = await Immobile.findAll({
      raw: true,
      where: {
        id_immobile: {
          [Op.notIn] : [ref_immobili_prenotati]
        },
        città_immobile: città,
        provincia_immobile : provincia,
        regione_immobile: regione,
        tipo_struttura : tipo_alloggio,
        posti_letto: {
          [Op.gte] : ospiti
        },
        costo_notte: {
          [Op.lte] : costo
        }
      }
    }) 
  } catch(err) {
    return err;
  }
    immobili_liberi.forEach(immobile => {
    if(new Date(immobile.data_inizio_spons) <= data_odierna && data_odierna <= new Date(immobile.data_fine_spons)) {
        immobile.isSponsorizzata = true;
    }   
    else {
        immobile.isSponsorizzata = false;
    }
 
}); 
  
  return immobili_liberi; 
}
 
Immobile.OttieniInfoHost = async function(idImmobile){
  return await Immobile.findOne({
    attributes: [],
    where: {id_immobile: idImmobile},
    include: [{
        model: Utente,
        attributes: ['nome', 'cognome', 'email', 'telefono']
            }]
  });
}
 
Immobile.OttieniCostoNotte = async function(idImmobile){
  return await Immobile.findOne({
    attributes: ['costo_notte'],
    where: {id_immobile: idImmobile}
  });
}

Immobile.ottieniImmobiliById = async function(idUtente){
  return await Immobile.findAll({attributes: ['id_immobile', 'nome_immobile'], where: {ref_proprietario: idUtente}});
}
Immobile.getImmobile = async function(idImmobile){
  return await Immobile.findOne({ attributes: ['nome_immobile', 'città_immobile', 
  'provincia_immobile', 'regione_immobile', 'indirizzo_immobile', 'numero_stanze', 
  'posti_letto', 'numero_bagni', 'descrizione', 'punti_interesse', 'tipo_struttura'],
    where: {id_immobile: idImmobile},
    include: [{ 
      model: Composta, 
      attributes: ['ref_servizio'],
      include: [{model: Servizio, attributes: ['descrizione']}]}
    ]
  });
}

Immobile.ottieniProprietario = async function(idImmobile){
  return await Immobile.findOne({attributes: ['ref_proprietario'], where: {id_immobile: idImmobile}})
}
 
Immobile.modificaImmobile = async function(idImmobile, nome_immobile, citta, provincia, regione, indirizzo, costo, numero_stanze, posti_letto, numero_bagni, descrizione, punti_interesse){
  return await Immobile.update({
    updatedAt: Date.now(),
    nome_immobile: nome_immobile, 
    città_immobile : citta,
    provincia_immobile : provincia,
    regione_immobile : regione,
    indirizzo_immobile : indirizzo,
    costo_notte : costo,
    numero_stanze : numero_stanze,
    posti_letto : posti_letto,
    numero_bagni: numero_bagni,
    descrizione: descrizione, 
    punti_interesse: punti_interesse
  }, {where: {id_immobile : idImmobile}});
}
 
Immobile.creaImmobile = async function(idImmobile, idUtente, nome_immobile, citta, provincia, regione, indirizzo, costo, numero_stanze, posti_letto, num_bagni, descrizione, tipo_struttura, punti_interesse){
  return await  Immobile.create({id_immobile: idImmobile, città_immobile: citta, provincia_immobile: provincia, 
    regione_immobile: regione, indirizzo_immobile: indirizzo, costo_notte: costo, numero_stanze: numero_stanze,
    posti_letto: posti_letto, numero_bagni:num_bagni, ref_proprietario: idUtente, nome_immobile: nome_immobile, descrizione: descrizione, tipo_struttura: tipo_struttura, punti_interesse: punti_interesse});
}
 
Immobile.sponsorizzaImmobile = async function(idImmobile, dataOdierna, dataFine){
  return await Immobile.update({data_inizio_spons : dataOdierna, data_fine_spons: dataFine}, {where:{id_immobile : idImmobile}});
}
 
Immobile.eliminaImmobile = async function(idImmobile){
  return await Immobile.destroy({where: {id_immobile: idImmobile}});
}
  

Immobile.hasMany(Prenotazione, {foreignKey : 'ref_immobile'});
Prenotazione.belongsTo(Immobile, {foreignKey : 'ref_immobile', sourceKey: 'ref_immobile'});

Immobile.belongsTo(Utente, {foreignKey: 'ref_proprietario', sourceKey: 'ref_proprietario'});
Utente.hasMany(Immobile, {foreignKey: 'ref_proprietario'});

module.exports = Immobile;