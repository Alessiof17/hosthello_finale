const Sequelize = require('sequelize');
const db = require('../config/database');
const Immobile = require('./immobile');
const Servizio = require('./servizio');

const Composta = db.define('composta', {
    ref_immobile: {
      type: Sequelize.STRING(100),
      allowNull: false,
      primaryKey: true,
      references: {
        model: Immobile,
        key: 'id_immobile'
      }
    },
    ref_servizio: {
      type: Sequelize.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: Servizio,
        key: 'id_servizio'
      }
    }
  }, {
    tableName: 'composta'
});

Composta.OttieniImmobileById = async function(idImmobile){
  return await Immobile.findOne({
    where: {id_immobile: idImmobile},
    include: [{ 
      model: Composta, 
      attributes: ['ref_servizio'],
      include: [{model: Servizio, attributes: ['descrizione']}]}
    ]
  });
}

Composta.inserisciServizio = async function(idImmobile, idServizio){
  return await Composta.create({ref_immobile: idImmobile, ref_servizio: idServizio});
}
 
Composta.rimuoviServizi = async function(idImmobile){
  return await Composta.destroy({where: {ref_immobile: idImmobile}});
}

Immobile.hasMany(Composta, {foreignKey: 'ref_immobile'});
Composta.belongsTo(Immobile, {foreignKey: 'ref_immobile', sourceKey: 'ref_immobile'});
Servizio.hasMany(Composta, {foreignKey: 'ref_servizio'});
Composta.belongsTo(Servizio, {foreignKey: 'ref_servizio', sourceKey: 'ref_servizio'})

module.exports = Composta;

